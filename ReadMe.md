# rtsp_colorize
从rtsp服务器获得对齐的视频，进行点云着色，发给渲染模块。
## 运行环境:

  Ubuntu18.04

## 安装依赖库:

### 安装ecal
````shell
sudo add-apt-repository ppa:ecal/ecal-latest
sudo apt-get update
sudo apt-get install ecal
````

### 安装ffmpeg
````shell
sudo apt install libx264-dev

cd third_party/ffmpeg-4.4.1
./configure  --prefix=./build        \
            --enable-gpl         \
            --enable-version3    \
            --enable-nonfree     \
            --disable-static     \
            --enable-shared      \
            --enable-libx264    \
            --enable-libx265    \
            --disable-debug      \
            --enable-libfreetype \
            --enable-openssl     \
            --disable-x86asm \
            --disable-lzma
make -j8
make install
cd ../../
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./build/lib
````

### 安装opencv


````shell
wget https://github.com/opencv/opencv/archive/refs/tags/3.4.15.tar.gz
tar -xzvf 3.4.15.tar.gz
cd 3.4.15
mkdir build
cd build
cmake-gui 
make -j12
sudo make install
```` 
cmake-gui 选择如下模块： core calib3d imgproc features2d


## 编译:
````shell
cd rtsp_client
mkdir build
cd build
cmake ..
make 

```` 
