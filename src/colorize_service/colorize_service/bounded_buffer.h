#include <boost/circular_buffer.hpp>
#include <thread>
#include <mutex>

#include <condition_variable>
#include <functional>
#include <chrono> // for auto_cpu_timer

template <class T>
class BoundedBuffer
{
public:

  typedef boost::circular_buffer<T> container_type;
  typedef typename container_type::size_type size_type;
  typedef typename container_type::value_type value_type;

  explicit BoundedBuffer(size_type capacity) : m_unread(0), m_container(capacity) {}

  void push_front(const T&  item, int time_out_ms = 10)
  { // `param_type` represents the "best" way to pass a parameter of type `value_type` to a method.
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_not_full.wait_for(lock, std::chrono::milliseconds(time_out_ms), 
        std::bind(&BoundedBuffer<value_type>::is_not_full, this)
        ))
      return;
    m_container.push_front(item); 
    ++m_unread;
    lock.unlock();
    m_not_empty.notify_one();
  }

  void pop_back(value_type* pItem, int time_out_ms = 100) {  
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_not_empty.wait_for(lock, std::chrono::milliseconds(time_out_ms),
     std::bind(&BoundedBuffer<value_type>::is_not_empty, this)
     ))
      return;
    *pItem = m_container[--m_unread];
    lock.unlock();
    m_not_full.notify_one();
  }

private:
  BoundedBuffer(const BoundedBuffer&);              // Disabled copy constructor.
  BoundedBuffer& operator = (const BoundedBuffer&); // Disabled assign operator.

  bool is_not_empty() const { return m_unread > 0; }
  bool is_not_full() const { return m_unread < m_container.capacity(); }

  size_type m_unread;
  container_type m_container;
  std::mutex m_mutex;
  std::condition_variable m_not_empty;
  std::condition_variable m_not_full;
}; //