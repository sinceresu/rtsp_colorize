
#include "colorize_client.h"

  #include <termios.h>
  #include <unistd.h>
  #include <fcntl.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <vector>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

// #include "pcl_colorize/pcl_colorize_interface.h"
#include "pcl_colorize_interface.h"
// #include "pcl_colorize/libpcl_colorize.h"
#include "libpcl_colorize.h"

#include "image_processor.h"
#include "rtsp_client.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

static  const std::string base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

    bool is_base64(unsigned char c)
    {
        return (isalnum(c) || (c == '+') || (c == '/'));
    }

    std::string base64_encode(unsigned char const *bytes_to_encode, unsigned int in_len)
    {
        std::string ret;
        int i = 0;
        int j = 0;
        unsigned char char_array_3[3];
        unsigned char char_array_4[4];

        while (in_len--)
        {
            char_array_3[i++] = *(bytes_to_encode++);
            if (i == 3)
            {
                char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
                char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
                char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
                char_array_4[3] = char_array_3[2] & 0x3f;

                for (i = 0; (i < 4); i++)
                    ret += base64_chars[char_array_4[i]];
                i = 0;
            }
        }

        if (i)
        {
            for (j = i; j < 3; j++)
                char_array_3[j] = '\0';

            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

            for (j = 0; (j < i + 1); j++)
                ret += base64_chars[char_array_4[j]];

            while ((i++ < 3))
                ret += '=';
        }

        return ret;
    }

    std::string base64_decode(std::string const &encoded_string)
    {
        size_t in_len = encoded_string.size();
        int i = 0;
        int j = 0;
        int in_ = 0;
        unsigned char char_array_4[4], char_array_3[3];
        std::string ret;

        while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
        {
            char_array_4[i++] = encoded_string[in_];
            in_++;
            if (i == 4)
            {
                for (i = 0; i < 4; i++)
                    char_array_4[i] = base64_chars.find(char_array_4[i]) & 0xff;

                char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
                char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
                char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

                for (i = 0; (i < 3); i++)
                    ret += char_array_3[i];
                i = 0;
            }
        }

        if (i)
        {
            for (j = 0; j < i; j++)
                char_array_4[j] = base64_chars.find(char_array_4[j]) & 0xff;

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

            for (j = 0; (j < i - 1); j++)
                ret += char_array_3[j];
        }

        return ret;
    }
using namespace std;

namespace colorize_service{

namespace {
typedef struct MatBuf {
  int rows;
  int cols;
  int type;
  float robot_tx;
  float robot_ty;
  float robot_tz;
  float robot_qx;
  float robot_qy;
  float robot_qz;
  float robot_qw;
  float camera_tx;
  float camera_ty;
  float camera_tz;
  float camera_qx;
  float camera_qy;
  float camera_qz;
  float camera_qw;
  int horizontal;
  int vertical;
  int width;
  int height;
  float fovx;
  float fovy;
}MatBuf;

}

ColorizeClient::ColorizeClient() :
transTool_("rtsp_colorize"),
ipc_ip_("127.0.0.1"),
ipc_port_("1000"),
reconnect_seconds_(0){
  
}

void ColorizeClient::SetParameters(const std::string& pcl_filepath, const std::string& calib_filepath, 
double min_celcius, double max_celcius, int reconnect_seconds ) {
  pcl_filepath_ = pcl_filepath;
  calib_filepath_ = calib_filepath;
  min_celcius_ = min_celcius;
  max_celcius_ = max_celcius;
  reconnect_seconds_ = reconnect_seconds;
}


int ColorizeClient::Start(const std::string& camera_id, const std::string& streaming_url, const std::string& ipc_ip, const std::string& ipc_port) {
  
  transTool_.publisher("/color_src_data","");
  // token_ = rpc_clinet_->getToken(ipc_ip, ipc_port,"test","test");
  LOG(INFO) << "start  colorize_service.";
  pcl_colorizer_ = pcl_colorize::CreatePclColorizer();
  if (0 != pcl_colorizer_->SetCalibrationFile(calib_filepath_))
    return -1;

  pcl::PointCloud<pcl::PointXYZ>::Ptr raw_map(new pcl::PointCloud<pcl::PointXYZ>() );
  pcl::io::loadPLYFile<pcl::PointXYZ>(pcl_filepath_, *raw_map);
  pcl_colorizer_->SetRawPointCloud(raw_map);

  image_processor_ = make_shared<ImageProcessor>(pcl_colorizer_);

  image_processor_->SetTempratureRange(min_celcius_, max_celcius_);
  image_processor_->SetImageReadyCallback(bind(&ColorizeClient::ColorizedImageCallback, this, placeholders::_1, placeholders::_2, placeholders::_3,
                                                placeholders::_4, placeholders::_5, placeholders::_6, placeholders::_7, placeholders::_8));

  stop_flag_ = false;

  rtsp_client_ = make_shared<RtspClient>(image_processor_);
  rtsp_client_->Start(streaming_url, reconnect_seconds_);

  image_processor_->Start();

  ipc_ip_ = ipc_ip;
  ipc_port_ = ipc_port;
  camera_id_ = camera_id;
  return 0;
}

void ColorizeClient::Stop( ) {
  stop_flag_ = true;
  if (connect_thread_.joinable())
    connect_thread_.join();
  
  image_processor_->Stop();
  rtsp_client_->Stop();

}

void ColorizeClient::SaveImage(bool b) {
    if (image_processor_)
    {
        image_processor_->SaveImage(b);
    }
}

void ColorizeClient::ColorizedImageCallback(const Eigen::Affine3f& camera_pose, const Eigen::Affine3f& robot_pose, const Eigen::Vector2i& ptz_angle,
                                            float fovx, float fovy, int width, int height, const cv::Mat& colored_img) {
  if (buffer_.empty()) {
    buffer_.resize(sizeof(MatBuf) + colored_img.rows * colored_img.cols * colored_img.elemSize());
  }

  Eigen::Vector3f camera_translation = camera_pose.translation();
  Eigen::Quaternionf camera_rotation(camera_pose.linear());
  Eigen::Vector3f camera_euler = camera_rotation.toRotationMatrix().eulerAngles(2, 1, 0);
  float camera_yaw = camera_euler[0];
  float camera_pitch = camera_euler[1];
  float camera_roll = camera_euler[2];
  Eigen::Vector3f robot_translation  = robot_pose.translation();
  Eigen::Quaternionf robot_rotation(robot_pose.linear());
  Eigen::Vector3f robot_euler = robot_rotation.toRotationMatrix().eulerAngles(2, 1, 0);
  float robot_yaw = robot_euler[0];
  float robot_pitch = robot_euler[1];
  float robot_roll = robot_euler[2];

  MatBuf* mat_buf = reinterpret_cast<MatBuf*> (&buffer_[0]);
  mat_buf->rows = colored_img.rows;
  mat_buf->cols = colored_img.cols;
  mat_buf->type = colored_img.type();

  mat_buf->robot_tx = robot_translation[0];
  mat_buf->robot_ty = robot_translation[1];
  mat_buf->robot_tz = robot_translation[2];
  mat_buf->robot_qx = robot_rotation.x();
  mat_buf->robot_qy = robot_rotation.y();
  mat_buf->robot_qz = robot_rotation.z();
  mat_buf->robot_qw = robot_rotation.w();

  mat_buf->camera_tx = camera_translation[0];
  mat_buf->camera_ty = camera_translation[1];
  mat_buf->camera_tz = camera_translation[2];
  mat_buf->camera_qx = camera_rotation.x();
  mat_buf->camera_qy = camera_rotation.y();
  mat_buf->camera_qz = camera_rotation.z();
  mat_buf->camera_qw = camera_rotation.w();

  mat_buf->horizontal = ptz_angle[0];
  mat_buf->vertical = ptz_angle[1];

  mat_buf->width = width;
  mat_buf->height = height;
  mat_buf->fovx = fovx;
  mat_buf->fovy = fovy;
  
  memcpy(&buffer_[sizeof(MatBuf)], colored_img.ptr(), colored_img.rows * colored_img.cols * colored_img.elemSize());

  std::string tempData;

  tempData = base64_encode((unsigned char*)&buffer_[0], buffer_.size());

  rapidjson::StringBuffer buf;
  rapidjson::Writer<rapidjson::StringBuffer> writer(buf);

  writer.StartObject();
  writer.Key("cache");writer.String("false");
  writer.Key("unit_id");writer.String("-1");
  writer.Key("equip_id");writer.String(camera_id_.c_str());
  writer.Key("gateway_id"); writer.String("-1");
  writer.Key("token");writer.String("");
  writer.Key("replay");writer.String("false");
  writer.Key("arraytype");writer.String("bin");
  writer.Key("invalidation_time");writer.String("0");
  writer.Key("zip");writer.String("");
  writer.Key("datatype");writer.String("color_src_data");
  writer.Key("data");writer.String(tempData.c_str());
  writer.EndObject();

  std::string s = std::string( buf.GetString() );

  transTool_.publisher("/color_src_data",s);
  LOG(INFO) << "transtools send, camera_id:" << camera_id_;
  }
}
