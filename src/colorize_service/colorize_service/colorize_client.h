#pragma once

#include <string>
#include <memory>
#include <vector>
#include <thread>

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>

#include "transTool/include/transTool.h"

namespace pcl_colorize{
class PclColorizeInterface; 

}
namespace  colorize_service{
class RtspClient;
class ImageProcessor;

class ColorizeClient {
 public:
    enum ColorizeResult {
        SUCCESS = 0,
        COLORING_ERROR = -1,
        IO_ERROR = -2,
        BUSY = -3,
        INVALID_PARAM = -4,
        ERROR = -5
    };
    
  ColorizeClient();
  ~ColorizeClient(){};

  ColorizeClient(const ColorizeClient&) = delete;
  ColorizeClient& operator=(const ColorizeClient&) = delete;
  
  void SetParameters(const std::string& pcl_filepath, const std::string& calib_filepath, double min_celcius, double max_celcius, int reconnect_seconds = 0);
  
  int Start(const std::string& camera_id, const std::string& streaming_url, const std::string& ipc_ip, const std::string& ipc_port);
  void Stop();
  void SaveImage(bool b);
  private:
    enum ColorizeState {
        IDLE = 0,
        PAUSING = 1,
        COLORING =2,
    };

    void GetParameters();

    void ColorizedImageCallback(const Eigen::Affine3f& camera_pose, const Eigen::Affine3f& robot_pose, const Eigen::Vector2i& ptz_angle,
                                float fovx, float fovy, int width, int height, const cv::Mat& colored_img);

    std::string pcl_filepath_;
    std::string calib_filepath_;

    std::string camera_id_;
    std::string streaming_url_;
    double min_celcius_;
    double max_celcius_;
    int reconnect_seconds_ = -1;;

    bool stop_flag_ = false;
    std::shared_ptr<pcl_colorize::PclColorizeInterface> pcl_colorizer_;
    std::shared_ptr<ImageProcessor> image_processor_;
    std::shared_ptr<RtspClient> rtsp_client_;
    transTool::transTool transTool_;

    std::vector<char> buffer_;
    std::string ipc_ip_;
    std::string ipc_port_;

    std::string token_;
    std::thread connect_thread_;
  };
  
}
