
#include "colorize_service.h"


#include "gflags/gflags.h"
#include "glog/logging.h"


#include "colorize_client.h"



using namespace std;

namespace colorize_service{

namespace {

}

ColorizeService::ColorizeService()
{
  
}

void ColorizeService::SetParameters(const std::string&camera_id, const std::string& pcl_filepath, const std::string& calib_filepath,  
double min_celcius, double max_celcius, int reconnect_seconds) {
  if (camera_id.empty())
    return;
  CameraParameter parameter = {
    pcl_filepath, calib_filepath, min_celcius, max_celcius, reconnect_seconds
  };
  camera_parameters_[camera_id] = parameter;

}

bool ColorizeService::GetParameters(const std::string& camera_id, std::string& pcl_filepath, std::string& calib_filepath,  
    double& min_celcius, double& max_celcius, int& reconnect_seconds) {
  if (camera_parameters_.count(camera_id) == 0) {
    LOG(WARNING) << camera_id << "'s parameters have not been set , set parameters first!!";
    return false;
  }

  pcl_filepath = camera_parameters_[camera_id].pcl_filepath;
  calib_filepath = camera_parameters_[camera_id].calib_filepath;
  min_celcius = camera_parameters_[camera_id].min_celcius;
  max_celcius = camera_parameters_[camera_id].max_celcius;
  reconnect_seconds = camera_parameters_[camera_id].reconnect_seconds;

  return true;
}


int ColorizeService::Start(const std::string& camera_id, const std::string& streaming_url, const std::string& ipc_ip, const std::string& ipc_port) {
  if (colorize_clients_.count(camera_id) != 0) {
    LOG(WARNING) << camera_id << " has been started!!";
    return -1;
  }

  if (camera_parameters_.count(camera_id) == 0) {
    LOG(WARNING) << camera_id << "'s parameters have not been set , set parameters first!!";
    return -1;
  }

  colorize_clients_[camera_id] = make_shared<ColorizeClient>();
  colorize_clients_[camera_id]->SetParameters(camera_parameters_[camera_id].pcl_filepath, camera_parameters_[camera_id].calib_filepath, 
      camera_parameters_[camera_id].min_celcius, camera_parameters_[camera_id].max_celcius, camera_parameters_[camera_id].reconnect_seconds);

  return colorize_clients_[camera_id]->Start(camera_id, streaming_url, ipc_ip, ipc_port);

}

void ColorizeService::Stop(const std::string& camera_id ) {
  if (colorize_clients_.count(camera_id) == 0) {
    LOG(WARNING) << camera_id << " has not been started!!";
    return;
  }
  colorize_clients_[camera_id]->Stop();
  colorize_clients_.erase(camera_id);
}

void ColorizeService::SaveImage(const std::string& camera_id, bool b) {
  if (colorize_clients_.count(camera_id) == 0) {
    LOG(WARNING) << camera_id << " has not been started!!";
    return;
  }
  colorize_clients_[camera_id]->SaveImage(b);
}

}
