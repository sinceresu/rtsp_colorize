#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>

#include <opencv2/opencv.hpp>


namespace  colorize_service{
class ColorizeClient;

class ColorizeService {
 public:
    enum ColorizeResult {
        SUCCESS = 0,
        COLORING_ERROR = -1,
        IO_ERROR = -2,
        BUSY = -3,
        INVALID_PARAM = -4,
        ERROR = -5
    };
    
  ColorizeService();
  ~ColorizeService(){};

  ColorizeService(const ColorizeService&) = delete;
  ColorizeService& operator=(const ColorizeService&) = delete;

  void SetParameters(const std::string&camera_id, const std::string& pcl_filepath, const std::string& calib_filepath,  
    double min_celcius, double max_celcius, int reconnect_seconds = 2);

  bool GetParameters(const std::string& camera_id, std::string& pcl_filepath, std::string& calib_filepath,  
    double& min_celcius, double& max_celcius, int& reconnect_seconds);

  int Start(const std::string& camera_id, const std::string& streaming_url, const std::string& ipc_ip, const std::string& ipc_port);
  void Stop(const std::string& camera_id);
  void SaveImage(const std::string& camera_id, bool b);
  private:
    typedef struct CameraParameter_s {
      std::string pcl_filepath;
      std::string calib_filepath;
      double min_celcius;
      double max_celcius;
      int reconnect_seconds;
    }CameraParameter;

    std::map<std::string,std::shared_ptr<ColorizeClient>> colorize_clients_;
    std::map<std::string,CameraParameter> camera_parameters_;

  };
  
}
