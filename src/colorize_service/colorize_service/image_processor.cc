#include "image_processor.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <cstdlib> 
#include <boost/filesystem.hpp>

#include <pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"
// #include "pcl_colorize/pcl_colorize_interface.h"
#include "pcl_colorize_interface.h"


// #define OUTPUT_PCL_FILES

using namespace std;

namespace colorize_service{

namespace {
void DequantizeTempertureImage( const cv::Mat& quantized_img, cv::Mat& temperture_image, double min_celcius, double max_celcius) {
  const double alpha_back = (max_celcius - min_celcius) / std::numeric_limits<uint16_t>::max();
  const double beta_back =  min_celcius;
  quantized_img.convertTo(temperture_image, CV_32FC1, alpha_back, beta_back);
    
}
uint16_t ShuffleByte(uint8_t even_byte, uint8_t odd_byte) {
  uint16_t result = 0u;
  for (size_t i = 0; i < 8; i++) {
    uint16_t combined = (even_byte & 0x1u ) + ((odd_byte & 0x1u ) << 1);
    result |= combined << (i * 2);
    even_byte = even_byte >> 1;
    odd_byte = odd_byte >> 1;
  }
  return result;
}
uint16_t ShuffleHalfByte(uint8_t even_half_byte, uint8_t odd_half_byte) {
  uint16_t result = 0u;
  for (size_t i = 0; i < 4; i++) {
    uint8_t combined = (even_half_byte & 0x1u ) + ((odd_half_byte & 0x1u ) << 1);
    result |= combined << (i * 2);
    even_half_byte = even_half_byte >> 1;
    odd_half_byte = odd_half_byte >> 1;
  }
  return result;
}

void ConvertYuv422ToQuantizedImage(const vector<cv::Mat> & yuv422_img, cv::Mat& quantized_img) {
  const cv::Mat& y_img = yuv422_img[0];
  const cv::Mat& u_img = yuv422_img[1];
  const cv::Mat& v_img = yuv422_img[2];


  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {
      // uint16_t value1 = (u_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col);
      quantized_img.at<uint16_t>(row, col) = ShuffleByte(y_img.at<uint8_t>(row, col), u_img.at<uint8_t>(row, col/2));
      // uint16_t value2 = (v_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col + 1);
      quantized_img.at<uint16_t>(row, col + 1) = ShuffleByte(y_img.at<uint8_t>(row, col + 1), v_img.at<uint8_t>(row, col/2));
    }
  }

}


void ConvertYuv444ToQuantizedImage(const vector<cv::Mat> & yuv_img, cv::Mat& quantized_img) {
  const cv::Mat& y_img = yuv_img[0];
  const cv::Mat& u_img = yuv_img[1];
  const cv::Mat& v_img = yuv_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col ++) {
      // uint16_t value1 = (u_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col);
      // uint8_t uv_shuffled = = ShuffleHalfByte(u_img.at<uint8_t>(row, col), v_img.at<uint8_t>(row, col));
      // uint16_t value2 = (v_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col + 1);
      quantized_img.at<uint16_t>(row, col) = ShuffleByte(y_img.at<uint8_t>(row, col ), u_img.at<uint8_t>(row, col));
    }
  }

}

void ConvertYuv42210pToQuantizedImage(const vector<cv::Mat> & yuv_img, cv::Mat& quantized_img) {
  const cv::Mat& y_img = yuv_img[0];
  const cv::Mat& u_img = yuv_img[1];
  const cv::Mat& v_img = yuv_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col ++) {
      // uint16_t value1 = (u_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col);
      // uint8_t uv_shuffled = = ShuffleHalfByte(u_img.at<uint8_t>(row, col), v_img.at<uint8_t>(row, col));
      // uint16_t value2 = (v_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col + 1);
        quantized_img.at<uint16_t>(row, col) = y_img.at<uint16_t>(row, col ) << 6;

    }
  }

}

void ConvertYuv444p10ToQuantizedImage(const vector<cv::Mat> & yuv_img, cv::Mat& quantized_img) {
  const cv::Mat& y_img = yuv_img[0];
  // const cv::Mat& u_img = yuv_img[1];
  // const cv::Mat& v_img = yuv_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col ++) {
      // uint16_t value1 = (u_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col);
      // uint8_t uv_shuffled = = ShuffleHalfByte(u_img.at<uint8_t>(row, col), v_img.at<uint8_t>(row, col));
      // uint16_t value2 = (v_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col + 1);
        quantized_img.at<uint16_t>(row, col) = y_img.at<uint16_t>(row, col ) << 6;

    }
  }
}
}



ImageProcessor::ImageProcessor(std::shared_ptr<pcl_colorize::PclColorizeInterface> pcl_colorizer)
: min_celcius_(0.),
max_celcius_(800.),
bounded_buffer_(10), 
pcl_colorizer_(pcl_colorizer),
fovx_(0.f),
fovy_(0.f),
width_(0),
height_(0),
stop_flag_(false)
{

}
 ImageProcessor::~ImageProcessor(){
   Stop();
 }

int ImageProcessor::SendPosedImage(const PosedImage& image) {
  bounded_buffer_.push_front(image);
  return 0;
}
int ImageProcessor::Start(){

  work_thread_ = thread(bind(&ImageProcessor::Run, this));
  return 0;
}

void ImageProcessor::Stop(){
  stop_flag_ = true;
  if (work_thread_.joinable())
    work_thread_.join();
}


void ImageProcessor::SetTempratureRange(double min_celcius, double max_celcius) {

  min_celcius_ = min_celcius;
  max_celcius_ = max_celcius;
}

void ImageProcessor::SetImageReadyCallback(ImageReadyCallback call_back) {
  image_ready_cb_ = call_back;
}

void TempToJet(const cv::Mat &temp, float celsius_min, float celsius_max,
                               cv::Mat &color) {

  const double alpha = 255.0 / (celsius_max - celsius_min);
  const double beta = -alpha * celsius_min;

  temp.convertTo(color, CV_8UC1, alpha, beta);
  cv::applyColorMap(color, color, cv::COLORMAP_JET);
}
cv::Mat ConvertDepthToJet(const cv::Mat& image) {
  cv::Mat temp_image(image.rows, image.cols, CV_32FC1);
  cv::Mat jet_image;
  for (int row = 0; row < image.rows; ++row){
    for (int col = 0; col < image.cols; ++col){
      const cv::Vec4f & pixel = image.at<cv::Vec4f>(row, col);
      temp_image.at<float>(row, col) = pixel[0];
    }
  }
  TempToJet(temp_image, 0, 30, jet_image);
   return jet_image;
}


void ConvertDepthToPcl(const cv::Mat& image, const cv::Mat& jet_image, pcl::PointCloud<pcl::PointXYZRGB>::Ptr & output) {
  for (int row = 0; row < image.rows; ++row){
    for (int col = 0; col < image.cols; ++col){
      const cv::Vec4f & pixel = image.at<cv::Vec4f>(row, col);
      const cv::Vec3b & bgr_pixel = jet_image.at<cv::Vec3b>(row, col);
      // if (row == 90 && col == 45 ) {
      //     LOG(INFO) << "rgb." << (int)bgr_pixel[2]  << ", " << (int)bgr_pixel[1]  << ", " << (int)bgr_pixel[0] ;
      //     LOG(INFO) << "xyz." <<pixel[1]  << ", " <<pixel[2]  << ", " <<pixel[3] ;
      // }
      if (pixel[1] > 10000) {
        continue;
      }

      pcl::PointXYZRGB point(bgr_pixel[2], bgr_pixel[1], bgr_pixel[0]);
      point.x = pixel[1];
      point.y = pixel[2];
      point.z = pixel[3];

      output->push_back(point);
    }
  }
}

void ImageProcessor::Run(){
  while(!stop_flag_) {
    PosedImage posed_image;
    bounded_buffer_.pop_back(&posed_image);
    if (posed_image.yuv_img.empty()) {
      continue;
    }
    cv::Mat quantized_img(posed_image.yuv_img[0].rows, posed_image.yuv_img[0].cols, CV_16UC1);

    ConvertYuv444p10ToQuantizedImage(posed_image.yuv_img, quantized_img);


    cv::Mat temp_image;
    DequantizeTempertureImage(quantized_img, temp_image, min_celcius_, max_celcius_);

    cv::Mat colored_image;
    if (0 != pcl_colorizer_->Colorize(temp_image, posed_image.camera.cast<float>(), colored_image))
      return;
    if (image_ready_cb_) {
      double fovx, fovy;
      pcl_colorizer_->GetCameraFov(fovx, fovy);
      fovx_ = (float)fovx;
      fovy_ = (float)fovy;
      cv::Size camera_size;
      pcl_colorizer_->GetCameraSize(camera_size);
      width_ = camera_size.width;
      height_ = camera_size.height;
    
      image_ready_cb_(posed_image.camera.cast<float>(), posed_image.robot.cast<float>(), posed_image.ptz,
                      fovx_, fovy_, width_, height_, colored_image);
    }
    // test
    {
      if (b_save_image_)
      {
        static int i = 0;

        cv::Mat jet_image = ConvertDepthToJet(colored_image);
        std::string jet_file = "./Image/jet_output_" + std::to_string(i) + ".png";
        cv::imwrite(jet_file, jet_image);
        std::string pose_file = "./Image/pose_output_" + std::to_string(i) + ".txt";
        std::ofstream pose_stream(pose_file);
        pose_stream << posed_image.camera.matrix();
        pose_stream.close();

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr  output_pcl(new (pcl::PointCloud<pcl::PointXYZRGB>));
        ConvertDepthToPcl(colored_image, jet_image, output_pcl);
        if (!output_pcl->points.empty()) {
          std::string pcd_file = "./Image/pcd_output_" + std::to_string(i) + ".pcd";
          pcl::io::savePCDFileBinary(pcd_file, *output_pcl);
        }
        i++;
      }
    }
  }
}

void ImageProcessor::SaveImage(bool b){
  b_save_image_ = b;
}

} // namespace colorize_service
