#pragma once

#include <vector>
#include <memory>
#include <thread>
#include <mutex>
#include <functional>

#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "bounded_buffer.h"
namespace pcl_colorize {
  class PclColorizeInterface; 
}

namespace colorize_service{

struct PosedImage {
  std::vector<cv::Mat> yuv_img;
  Eigen::Affine3d camera;
  Eigen::Affine3d robot;
  Eigen::Vector2i ptz;
};

using ImageReadyCallback = std::function<void(const Eigen::Affine3f&, const Eigen::Affine3f&, const Eigen::Vector2i&, float, float, int, int, const cv::Mat&)>;

class ImageProcessor  
{
public:
  ImageProcessor(std::shared_ptr<pcl_colorize::PclColorizeInterface> pcl_colorizer);
  ~ImageProcessor();
  int SendPosedImage(const PosedImage& image);
  void SetTempratureRange(double min_celcius, double max_celcius);
  void SetImageReadyCallback(ImageReadyCallback call_back);

  int Start();
  void Stop();
  
  // virtual  int  ColorizeVideoFrames(std::vector<std::vector<std::string>>&video_filenames, const std::vector<Eigen::Matrix4f> & track_poses, , const std::vector<double> &  offset_secs, const std::string & save_directory,  const std::string& map_name) override ;
private:
  void Run();
  std::mutex m_mutex_ ;

  double min_celcius_;
  double max_celcius_;


  bool stop_flag_;


  BoundedBuffer<PosedImage> bounded_buffer_;
  std::shared_ptr<pcl_colorize::PclColorizeInterface> pcl_colorizer_;
  bool initialized_ = false;

  std::thread work_thread_;

  ImageReadyCallback image_ready_cb_;

  float fovx_;
  float fovy_;
  int width_;
  int height_;

  private:
    bool b_save_image_ = false;
  public:
    void SaveImage(bool b);

};
} // namespace colorize_service

