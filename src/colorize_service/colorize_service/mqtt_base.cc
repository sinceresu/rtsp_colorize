#include "mqtt_base.h"

CMqttBase::CMqttBase()
    : check_status(false)
{
  std::thread monitor(std::bind(&CMqttBase::connect_monitor, this, nullptr));
  monitor.detach();
}

CMqttBase::~CMqttBase()
{
}

std::time_t CMqttBase::get_time_stamp()
{
  // std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
  // std::time_t timeStamp = tp.time_since_epoch().count();
  //
  std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
  std::time_t timeStamp = ms.count();
  //
  return timeStamp;
}

int CMqttBase::md5_encoded(const char *input, char *output)
{
  int nRet = 0;
  //
  unsigned char md[MD5_DIGEST_LENGTH];
  memset(md, 0, MD5_DIGEST_LENGTH);
  char buf[MD5_DIGEST_LENGTH * 2 + 1];
  memset(buf, 0, MD5_DIGEST_LENGTH * 2 + 1);
  //
  MD5_CTX ctx;
  // 1. 初始化
  if (1 != MD5_Init(&ctx))
  {
    nRet = -1;
    printf("MD5_Init failed ... \r\n");
    return -1;
  }
  // 2. 添加数据
  if (1 != MD5_Update(&ctx, (const void *)input, strlen((char *)input)))
  {
    nRet = -1;
    printf("MD5_Update failed ... \r\n");
    return -1;
  }
  // 3. 计算结果
  if (1 != MD5_Final(md, &ctx))
  {
    nRet = -1;
    printf("MD5_Final failed ... \r\n");
    return -1;
  }
  // 4. 输出结果
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
  {
    sprintf(&buf[i * 2], "%02x", md[i]);
    // sprintf(&buf[i * 2], "%02X", md[i]);
  }
  if (output == nullptr)
  {
    nRet = -1;
    printf("output nullptr ... \r\n");
    return -1;
  }
  strcpy(output, buf);
  //
  // MD5((unsigned char *)input, strlen((char *)input), md);
  // for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
  // {
  //   sprintf(&buf[i * 2], "%02x", md[i]);
  //   // sprintf(&buf[i * 2], "%02X", md[i]);
  // }
  // strcpy(output, buf);

  return nRet;
}

char *CMqttBase::strtok_hier(char *str, char **saveptr)
{
  char *c;

  if (str != NULL)
  {
    *saveptr = str;
  }

  if (*saveptr == NULL)
  {
    return NULL;
  }

  c = strchr(*saveptr, '/');
  if (c)
  {
    str = *saveptr;
    *saveptr = c + 1;
    c[0] = '\0';
  }
  else if (*saveptr)
  {
    /* No match, but surplus string */
    str = *saveptr;
    *saveptr = NULL;
  }
  return str;
}

int CMqttBase::count_hier_levels(const char *s)
{
  int count = 1;
  const char *c = s;

  while ((c = strchr(c, '/')) && c[0])
  {
    c++;
    count++;
  }
  return count;
}

bool CMqttBase::hash_check(char *s, size_t *len)
{
  if ((*len) == 1 && s[0] == '#')
  {
    s[0] = '\0';
    (*len)--;
    return true;
  }
  else if ((*len) > 1 && s[(*len) - 2] == '/' && s[(*len) - 1] == '#')
  {
    s[(*len) - 2] = '\0';
    s[(*len) - 1] = '\0';
    (*len) -= 2;
    return true;
  }
  return false;
}

bool CMqttBase::sub_acl_check(const char *acl, const char *sub)
{
  char *acl_local;
  char *sub_local;
  size_t acl_len, sub_len;
  bool acl_hash = false, sub_hash = false;
  int acl_levels, sub_levels;
  int i;
  char *acl_token, *sub_token;
  char *acl_saveptr, *sub_saveptr;

  acl_len = strlen(acl);
  if (acl_len == 1 && acl[0] == '#')
  {
    return true;
  }

  sub_len = strlen(sub);
  /* mosquitto_validate_utf8(acl, acl_len); */

  acl_local = strdup(acl);
  sub_local = strdup(sub);
  if (acl_local == NULL || sub_local == NULL)
  {
    free(acl_local);
    free(sub_local);
    return false;
  }

  acl_hash = hash_check(acl_local, &acl_len);
  sub_hash = hash_check(sub_local, &sub_len);

  if (sub_hash == true && acl_hash == false)
  {
    free(acl_local);
    free(sub_local);
    return false;
  }

  acl_levels = count_hier_levels(acl_local);
  sub_levels = count_hier_levels(sub_local);
  if (acl_levels > sub_levels)
  {
    free(acl_local);
    free(sub_local);
    return false;
  }
  else if (sub_levels > acl_levels)
  {
    if (acl_hash == false)
    {
      free(acl_local);
      free(sub_local);
      return false;
    }
  }

  acl_saveptr = acl_local;
  sub_saveptr = sub_local;
  for (i = 0; i < sub_levels; i++)
  {
    acl_token = strtok_hier(acl_saveptr, &acl_saveptr);
    sub_token = strtok_hier(sub_saveptr, &sub_saveptr);

    if (i < acl_levels &&
        (!strcmp(acl_token, "+") || !strcmp(acl_token, sub_token)))
    {

      /* This level matches a single level wildcard, or is an exact
       * match, so carry on checking. */
    }
    else if (i >= acl_levels && acl_hash == true)
    {
      /* The sub has more levels of hierarchy than the acl, but the acl
       * ends in a multi level wildcard so the match is fine. */
    }
    else
    {
      free(acl_local);
      free(sub_local);
      return false;
    }
  }

  free(acl_local);
  free(sub_local);
  return true;
}

void CMqttBase::connect_monitor(void *p)
{
  while (true)
  {
    sleep(6);
    //
    if (check_status)
    {
      int rc;
      if ((rc = MQTTClient_isConnected(client)) != MQTTCLIENT_SUCCESS)
      {
        /* code */
      }
      else
      {
        printf("Failed to is connected, return code %d\n", rc);
        rc = EXIT_FAILURE;
        //
        reconnection();
      }
    }
  }
}

void CMqttBase::delivered(void *context, MQTTClient_deliveryToken dt, void *ud)
{
  printf("Message with token value %d delivery confirmed\n", dt);
  // deliveredtoken = dt;
  if (ud)
  {
    CMqttBase *p_this = (CMqttBase *)ud;
    p_this->deliveredtoken = dt;
  }
}

int CMqttBase::msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message, void *ud)
{
  // printf("Message arrived\n");
  // printf("topic: %s\n", topicName);
  // printf("message: %.*s\n", message->payloadlen, (char *)message->payload);

  std::string message_data((char *)message->payload, 0, message->payloadlen);
  int message_length = message->payloadlen;

  if (ud)
  {
    CMqttBase *p_this = (CMqttBase *)ud;
    std::map<std::string, topic_t>::iterator topic;
    topic = p_this->sud_topic.find(topicName);
    if (topic != p_this->sud_topic.end())
    {
      topic->second.callback(topicName, message_data, message_length);
    }
    else
    {
      // Wildcard matching
      for (std::map<std::string, topic_t>::iterator topic = p_this->sud_topic.begin(); topic != p_this->sud_topic.end(); topic++)
      {
        if (strchr(topic->second.name.c_str(), '#') || strchr(topic->second.name.c_str(), '+'))
        {
        }
        else
        {
          continue;
        }

        if (p_this->topic_match(topic->second.name, topicName, true))
        {
          topic->second.callback(topicName, message_data, message_length);
        }
      }
    }
  }

  // if (strcmp(topicName, "device/updata") == 0)
  // {
  //   //
  // }

  MQTTClient_freeMessage(&message);
  MQTTClient_free(topicName);
  return 1;
}

void CMqttBase::connlost(void *context, char *cause, void *ud)
{
  printf("\nConnection lost\n");
  printf("     cause: %s\n", cause);

  if (ud)
  {
    CMqttBase *p_this = (CMqttBase *)ud;
    p_this->reconnection();
  }
}

int CMqttBase::init(std::string _address, std::string _clientId)
{
  address = _address;
  clientId = _clientId;
  int rc;
  if ((rc = MQTTClient_create(&client, address.c_str(), clientId.c_str(), MQTTCLIENT_PERSISTENCE_NONE, NULL)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to create client, return code %d\n", rc);
    rc = EXIT_FAILURE;
  }
  else
  {
    if ((rc = MQTTClient_setUserData(client, (void *)this)) != MQTTCLIENT_SUCCESS)
    {
      printf("Failed to set user data, return code %d\n", rc);
      rc = EXIT_FAILURE;
      return rc;
    }
    if ((rc = MQTTClient_setCallbacks(client, NULL, CMqttBase::connlost, CMqttBase::msgarrvd, CMqttBase::delivered)) != MQTTCLIENT_SUCCESS)
    {
      printf("Failed to set callbacks, return code %d\n", rc);
      rc = EXIT_FAILURE;
    }
  }

  return rc;
}

int CMqttBase::uninit()
{
  int rc;
  for (std::map<std::string, topic_t>::iterator topic = sud_topic.begin(); topic != sud_topic.end(); topic++)
  {
    if ((rc = MQTTClient_unsubscribe(client, topic->second.name.c_str())) != MQTTCLIENT_SUCCESS)
    {
      printf("Failed to unsubscribe, return code %d\n", rc);
      rc = EXIT_FAILURE;
    }
  }
  if ((rc = MQTTClient_disconnect(client, 10000)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to disconnect, return code %d\n", rc);
    rc = EXIT_FAILURE;
  }
  MQTTClient_destroy(&client);
  check_status = false;

  return rc;
}

int CMqttBase::connect(std::string _username, std::string _password)
{
  username = _username;
  password = _password;
  int rc;
  MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  conn_opts.username = username.c_str();
  conn_opts.password = password.c_str();
  if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to connect, return code %d\n", rc);
    rc = EXIT_FAILURE;
  }
  check_status = true;

  return rc;
}

int CMqttBase::reconnection()
{
  int rc;
  MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  conn_opts.username = username.c_str();
  conn_opts.password = password.c_str();
  if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to connect, return code %d\n", rc);
    rc = EXIT_FAILURE;
  }
  else
  {
    for (std::map<std::string, topic_t>::iterator topic = sud_topic.begin(); topic != sud_topic.end(); topic++)
    {
      if ((rc = MQTTClient_subscribe(client, topic->second.name.c_str(), topic->second.qos)) != MQTTCLIENT_SUCCESS)
      {
        printf("Failed to unsubscribe, return code %d\n", rc);
        rc = EXIT_FAILURE;
      }
    }
  }

  return rc;
}

int CMqttBase::publish(std::string _topicName, const int _qos, std::string _message)
{
  int rc;

  MQTTClient_deliveryToken token;
  MQTTClient_message pubmsg = MQTTClient_message_initializer;
  pubmsg.payload = (void *)_message.c_str();
  pubmsg.payloadlen = (int)strlen(_message.c_str());
  pubmsg.qos = _qos;
  pubmsg.retained = 0;
  // deliveredtoken = 0;
  if ((rc = MQTTClient_publishMessage(client, _topicName.c_str(), &pubmsg, &token)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to publish message, return code %d\n", rc);
  }
  else
  {
    rc = MQTTClient_waitForCompletion(client, token, 10000);
    printf("Message with delivery token %d delivered\n", token);

    // while (deliveredtoken != token)
    // {
    //     usleep(10000L);
    // }
  }

  return rc;
}

int CMqttBase::subscribe(std::string _topicName, int _qos, TopicMessageCallback _callback)
{
  topic_t topic;
  topic.name = _topicName;
  topic.qos = _qos;
  topic.callback = _callback;

  int rc;
  if ((rc = MQTTClient_subscribe(client, _topicName.c_str(), _qos)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to subscribe, return code %d\n", rc);
    rc = EXIT_FAILURE;
  }

  sud_topic[_topicName] = topic;

  return rc;
}

bool CMqttBase::topic_match(std::string acl, std::string sub, bool expected)
{
  bool result;
  result = sub_acl_check(acl.c_str(), sub.c_str());

  return (result == expected) ? true : false;
}