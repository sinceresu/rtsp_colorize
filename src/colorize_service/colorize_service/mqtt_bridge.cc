
#include "mqtt_bridge.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "colorize_service.h"

using namespace std;

int req_header(string &str_header)
{
  str_header = "";
  //
  time_t stamp = utils_common_libs::GetTimeStamp();
  string strStamp = to_string(stamp);
  //
  string temp_sign = "YDROBOT&&" + strStamp;
  //
  char md_c[16 * 2 + 1];
  if (0 != utils_common_libs::md5_encoded(temp_sign.c_str(), md_c))
  {
    str_header = "";
    return -1;
  }
  string strSign = md_c;
  //
  str_header = "timestamp: " + strStamp + "\r\n" + "sign: " + strSign + "\r\n";

  return 0;
}

namespace colorize_service
{

  namespace
  {

  }

  MqttBridge::MqttBridge()
  {
    colorize_service_ = std::make_shared<ColorizeService>();
  }

  int MqttBridge::http_init(std::string url)
  {
    std::string head;
    req_header(head);
    std::string getData;
    Json::Value root;
    // root["createTime"] = GetCurrentDate();
    root["type"] = 1;
    getData = root.toStyledString();
    utils_common_libs::CHttpClient HttpClient;
    HttpClient.get(url.c_str(), head.c_str(), getData.c_str(), getData.length(),
                   std::bind(&MqttBridge::response_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4), nullptr);

    return 0;
  }

  void MqttBridge::response_callback(int code, const char *response, int lenght, void *ud)
  {
    // printf("%d \r\n", code);
    // printf("%s \r\n", response);
    // printf("%d \r\n", lenght);

    Json::Reader reader;
    Json::Value root;
    if (!reader.parse(response, root))
    {
      std::cout << "json parse error." << std::endl;
      return;
    }
    else
    {
      if (root["data"].isObject() && !root["data"].isNull())
      {
        Json::Value cameras_array;
        cameras_array = root["data"]["list"];
        for (int i = 0; i < cameras_array.size(); i++)
        {
          Json::Value cItem;
          cItem = cameras_array[i];
          colorize_param_t colorize_param;
          // colorize_param.pcl_filepath = cItem["pcl_filepath"].asString();
          // colorize_param.calib_filepath = cItem["calib_filepath"].asString();
          // colorize_param.min_celcius = cItem["min_celcius"].asDouble();
          // colorize_param.max_celcius = cItem["max_celcius"].asDouble();
          // colorize_param.camera_id = cItem["camera_id"].asString();
          // colorize_param.rtsp_url = cItem["rtsp_url"].asString();
          //
          SetParameters(colorize_param.camera_id, colorize_param.pcl_filepath, colorize_param.calib_filepath, colorize_param.min_celcius, colorize_param.max_celcius);
          Start(colorize_param.camera_id, colorize_param.rtsp_url, ipc_ip, ipc_port);
        }
      }
    }
  }

  // mqtt
  int MqttBridge::MqttInit(std::string _address, std::string _clientId,
                           std::string _username, std::string _password)
  {
    // mqtt
    mqtt_base_ = make_shared<CMqttBase>();
    mqtt_base_->init(_address, _clientId);
    mqtt_base_->connect(_username, _password);

    MqttSubscribers();

    return 0;
  }

  void MqttBridge::MqttSubscribers()
  {
    utils_common_libs::CINIParser m_ini;
    m_ini.LoadFIle("config/config.ini");
    string CameraInfo = m_ini.GetData("mqtt_sub", "CameraInfo", "/camera/rtsp/param");
    string RtspConnect = m_ini.GetData("mqtt_sub", "RtspConnect", "/camera/rtsp/connect");

    mqtt_base_->subscribe(CameraInfo, 0, std::bind(&MqttBridge::CameraInfo_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    mqtt_base_->subscribe(RtspConnect, 0, std::bind(&MqttBridge::RtspConnect_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }

  int MqttBridge::Message_mqttPublish(std::string topic, std::string data)
  {
    Json::Reader reader;
    Json::Value data_json;
    if (!reader.parse(data, data_json))
    {
      std::cout << "json parse error" << std::endl;
      return -1;
    }
    time_t stamp = CMqttBase::get_time_stamp();
    std::string strStamp = std::to_string(stamp);
    std::string strLogId = strStamp;
    char md_c[32 + 1];
    if (CMqttBase::md5_encoded(strStamp.c_str(), md_c) == 0)
    {
      strLogId = md_c;
    }
    Json::Value mqtt_json;
    mqtt_json["logId"] = strLogId;
    mqtt_json["gatewayId"] = "";
    mqtt_json["timestamp"] = (Json::Int64)stamp;
    mqtt_json["type"] = 0;
    mqtt_json["version"] = MQTT_BASE_VERSION;
    mqtt_json["data"] = data_json;
    std::string mqtt_str = mqtt_json.toStyledString();

    mqtt_base_->publish(topic, 0, mqtt_str);

    return 0;
  }

  void MqttBridge::CameraInfo_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
  {
    printf("CameraInfo_mqttCallback - %s", _message.c_str());

    Json::Reader reader;
    Json::Value root;
    if (!reader.parse(_message, root))
    {
      std::cout << "json parse error." << std::endl;
      return;
    }
    else
    {
      std::string ipc_ip = root["ipc_ip"].asString();
      std::string ipc_port = root["ipc_port"].asString();
      //
      Json::Value cameras_array;
      cameras_array = root["Cameras"];
      for (int i = 0; i < cameras_array.size(); i++)
      {
        Json::Value cItem;
        cItem = cameras_array[i];
        colorize_param_t colorize_param;
        colorize_param.pcl_filepath = cItem["pcl_filepath"].asString();
        colorize_param.calib_filepath = cItem["calib_filepath"].asString();
        colorize_param.min_celcius = cItem["min_celcius"].asDouble();
        colorize_param.max_celcius = cItem["max_celcius"].asDouble();
        colorize_param.camera_id = cItem["camera_id"].asString();
        colorize_param.rtsp_url = cItem["rtsp_url"].asString();
        //
        SetParameters(colorize_param.camera_id, colorize_param.pcl_filepath, colorize_param.calib_filepath, colorize_param.min_celcius, colorize_param.max_celcius);
        Start(colorize_param.camera_id, colorize_param.rtsp_url, ipc_ip, ipc_port);
      }
    }
  }

  void MqttBridge::RtspConnect_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
  {
    printf("RtspConnect_mqttCallback \e[0;32m topicName:%s、message:%s ... \e[0m \r\n", _topicName.c_str(), _message.c_str());

    std::string data_str = "";
    Json::Reader reader;
    Json::Value root;
    if (reader.parse(_message, root))
    {
      if (root["data"].isNull())
      {
        return;
      }

      std::string pcl_filepath;
      std::string calib_filepath;
      std::string camera_id = root["data"]["camera_id"].asString();
      std::string rtsp_url = root["data"]["rtsp_url"].asString();
      double min_celcius = root["data"]["min_celcius"].asDouble();
      double max_celcius = root["data"]["max_celcius"].asDouble();
      int type = root["data"]["type"].asInt();
      if (type == 1)
      {
        std::ifstream fs("config/camera.json", std::ifstream::binary);
        if (!fs)
        {
          return;
        }
        std::string _tmp((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_tmp, root))
        {
          std::cout << "json parse error." << std::endl;
          return;
        }
        else
        {
          std::string _ipc_ip = root["ipc_ip"].asString();
          std::string _ipc_port = root["ipc_port"].asString();
          //
          Json::Value cameras_array;
          cameras_array = root["Cameras"];
          for (int i = 0; i < cameras_array.size(); i++)
          {
            Json::Value cItem;
            cItem = cameras_array[i];
            colorize_param_t colorize_param;
            colorize_param.pcl_filepath = cItem["pcl_filepath"].asString();
            colorize_param.calib_filepath = cItem["calib_filepath"].asString();
            colorize_param.min_celcius = cItem["min_celcius"].asDouble();
            colorize_param.max_celcius = cItem["max_celcius"].asDouble();
            colorize_param.camera_id = cItem["camera_id"].asString();
            colorize_param.rtsp_url = cItem["rtsp_url"].asString();
            //
            if (camera_id == colorize_param.camera_id)
            {
              SetParameters(colorize_param.camera_id, colorize_param.pcl_filepath, colorize_param.calib_filepath, colorize_param.min_celcius, colorize_param.max_celcius);
              Start(colorize_param.camera_id, colorize_param.rtsp_url, _ipc_ip, _ipc_port);
              return;
            }
          }
        }
      }
      else
      {
        Stop(camera_id);
      }

      // SetParameters(camera_id, pcl_filepath, calib_filepath, min_celcius, max_celcius);
      // Start(camera_id, rtsp_url, ipc_ip, ipc_port);
    }
  }

  int MqttBridge::Test_mqttPublish(std::string data)
  {
    Message_mqttPublish("/test", data);
    return 0;
  }
  //

  void MqttBridge::SetParameters(const std::string &camera_id, const std::string &pcl_filepath, const std::string &calib_filepath,
                                 double min_celcius, double max_celcius)
  {
    if (colorize_service_ == nullptr)
      return;
    colorize_service_->SetParameters(camera_id, pcl_filepath, calib_filepath, min_celcius, max_celcius);
  }

  int MqttBridge::Start(const std::string &camera_id, const std::string &streaming_url, const std::string &ipc_ip, const std::string &ipc_port)
  {
    if (colorize_service_ == nullptr)
      return -1;
    return colorize_service_->Start(camera_id, streaming_url, ipc_ip, ipc_port);
  }

  void MqttBridge::Stop(const std::string &camera_id)
  {
    if (colorize_service_ == nullptr)
      return;
    colorize_service_->Stop(camera_id);
  }

  void MqttBridge::SaveImage(const std::string &camera_id, bool b)
  {
    if (colorize_service_ == nullptr)
      return;
    colorize_service_->SaveImage(camera_id, b);
  }

  void MqttBridge::Run()
  {
    utils_common_libs::CINIParser m_ini;
    m_ini.LoadFIle("config/config.ini");
    string address = m_ini.GetData("mqtt", "address", "127.0.0.1:1883");
    string clientId = m_ini.GetData("mqtt", "clientId", "root");
    string username = m_ini.GetData("mqtt", "username", "ydrobot");
    string password = m_ini.GetData("mqtt", "password", "123qweasd");
    MqttInit(address, clientId, username, password);
    //
    // string http_camera = m_ini.GetData("http", "CameraInfo", "http://127.0.0.1:8080/ydrobot-station-gds-api-v4/ui/cameras");
    // http_init(http_camera);

    /* test */
    // std::ifstream fs("config/camera.json", std::ifstream::binary);
    // if (!fs)
    // {
    //   return;
    // }
    // std::string _tmp((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());

    // Json::Reader reader;
    // Json::Value root;
    // if (!reader.parse(_tmp, root))
    // {
    //   std::cout << "json parse error." << std::endl;
    //   return;
    // }
    // else
    // {
    //   std::string _ipc_ip = root["ipc_ip"].asString();
    //   std::string _ipc_port = root["ipc_port"].asString();
    //   //
    //   Json::Value cameras_array;
    //   cameras_array = root["Cameras"];
    //   for (int i = 0; i < cameras_array.size(); i++)
    //   {
    //     Json::Value cItem;
    //     cItem = cameras_array[i];
    //     colorize_param_t colorize_param;
    //     colorize_param.pcl_filepath = cItem["pcl_filepath"].asString();
    //     colorize_param.calib_filepath = cItem["calib_filepath"].asString();
    //     colorize_param.min_celcius = cItem["min_celcius"].asDouble();
    //     colorize_param.max_celcius = cItem["max_celcius"].asDouble();
    //     colorize_param.camera_id = cItem["camera_id"].asString();
    //     colorize_param.rtsp_url = cItem["rtsp_url"].asString();
    //     //
    //     SetParameters(colorize_param.camera_id, colorize_param.pcl_filepath, colorize_param.calib_filepath, colorize_param.min_celcius, colorize_param.max_celcius);
    //     Start(colorize_param.camera_id, colorize_param.rtsp_url, _ipc_ip, _ipc_port);
    //   }
    // }
  }

}
