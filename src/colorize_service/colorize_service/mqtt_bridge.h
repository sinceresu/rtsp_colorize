#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>

#include <opencv2/opencv.hpp>
#include <jsoncpp/json/json.h>

#include "Common.hpp"
#include "http_client.hpp"
#include "ini_parser.hpp"
#include "mqtt_base.h"

namespace colorize_service
{

  typedef struct _colorize_param_t
  {
    std::string pcl_filepath;
    std::string calib_filepath;
    double min_celcius;
    double max_celcius;
    std::string camera_id;
    std::string rtsp_url;
  } colorize_param_t;

  class ColorizeService;

  class MqttBridge
  {
  public:
    MqttBridge();
    ~MqttBridge(){};

    MqttBridge(const MqttBridge &) = delete;
    MqttBridge &operator=(const MqttBridge &) = delete;

  public:
    void SetParameters(const std::string &camera_id, const std::string &pcl_filepath, const std::string &calib_filepath,
                       double min_celcius, double max_celcius);

    int Start(const std::string &camera_id, const std::string &streaming_url, const std::string &ipc_ip, const std::string &ipc_port);
    void Stop(const std::string &camera_id);

    void SaveImage(const std::string &camera_id, bool b);

    void Run();

  private:
    std::shared_ptr<ColorizeService> colorize_service_;
    std::string ipc_ip;
    std::string ipc_port;

  private:
    int http_init(std::string url);
    void response_callback(int code, const char *response, int lenght, void *ud);

  private:
    std::shared_ptr<CMqttBase> mqtt_base_;
    int MqttInit(std::string _address, std::string _clientId, std::string _username, std::string _password);
    void MqttSubscribers();
    int Message_mqttPublish(std::string topic, std::string data);

    void CameraInfo_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);
    void RtspConnect_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);

    int Test_mqttPublish(std::string data);
  };

}
