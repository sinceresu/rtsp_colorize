
#include "rtsp_client.h"
#include <future>

#include <Eigen/Geometry>
#include <opencv2/opencv.hpp>

// #include "gflags/gflags.h"
#include "glog/logging.h"

extern "C" {
//#include "libavutil/avutil.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
};

#include "image_processor.h"



using namespace std;
using namespace cv;

namespace colorize_service{

namespace {

void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
};

template <class Type>  
Type stringToNum(const string& str)  
{  
    istringstream iss(str);  
    Type num;  
    iss >> num;  
    return num;      
} 

void ConvertFrameToYuvImg(AVFrame * frame, vector<Mat>& yuv_img ) {
  for(size_t y=0;y<frame->height;y++) {
    memcpy( (uint8_t*)yuv_img[0].ptr<uint16_t>(y), &frame->data[0][y * frame->linesize[0]], frame->width*sizeof(uint16_t));
    // memcpy( (uint8_t*)yuv_img[1].ptr<uint16_t>(y), &frame->data[1][y * frame->linesize[1]], frame->width*sizeof(uint16_t));
    // memcpy( (uint8_t*)yuv_img[2].ptr<uint16_t>(y), &frame->data[2][y * frame->linesize[2]], frame->width*sizeof(uint8_t));
  }
    // LOG(INFO) << "0, 0: " << yuv_img[0].at<uint16_t>(0, 0) << "  300, 0: " << yuv_img[0].at<uint16_t>(300, 0) ;  
}

bool ConvertSeiToPose(const string& sei_content,Eigen::Affine3d& camera, Eigen::Affine3d& robot, Eigen::Vector2i& ptz ) {
  vector<string> pose_list;
  SplitString(sei_content, pose_list, ";");
  if (pose_list.size() == 3) {
    // camera
    vector<string> camera_strs;
    SplitString(pose_list[0], camera_strs, ",");
    if (camera_strs.size() != 7)
      return false;
    camera.translation() = Eigen::Vector3d(stringToNum<double>(camera_strs[0]), 
                stringToNum<double>(camera_strs[1]), stringToNum<double>(camera_strs[2]));
    camera.linear() = Eigen::Quaterniond(stringToNum<double>(camera_strs[6]), 
                stringToNum<double>(camera_strs[3]), stringToNum<double>(camera_strs[4]),
                  stringToNum<double>(camera_strs[5])).matrix();
    // robot
    vector<string> robot_strs;
    SplitString(pose_list[1], robot_strs, ",");
    if (robot_strs.size() != 7)
      return false;
    robot.translation() = Eigen::Vector3d(stringToNum<double>(robot_strs[0]), 
                stringToNum<double>(robot_strs[1]), stringToNum<double>(robot_strs[2]));

    robot.linear() = Eigen::Quaterniond(stringToNum<double>(robot_strs[6]), 
                stringToNum<double>(robot_strs[3]), stringToNum<double>(robot_strs[4]),
                  stringToNum<double>(robot_strs[5])).matrix();
    // ptz
    vector<string> ptz_strs;
    SplitString(pose_list[2], ptz_strs, ",");
    if (ptz_strs.size() != 2)
      return false;
    ptz = Eigen::Vector2i(stringToNum<int>(ptz_strs[0]), stringToNum<int>(ptz_strs[1]));
  }
  else if (pose_list.size() == 1) {
    // camera
    vector<string> camera_strs;
    SplitString(pose_list[0], camera_strs, ",");
    if (camera_strs.size() != 7)
      return false;
    camera.translation() = Eigen::Vector3d(stringToNum<double>(camera_strs[0]), 
                stringToNum<double>(camera_strs[1]), stringToNum<double>(camera_strs[2]));
    camera.linear() = Eigen::Quaterniond(stringToNum<double>(camera_strs[6]), 
                stringToNum<double>(camera_strs[3]), stringToNum<double>(camera_strs[4]),
                  stringToNum<double>(camera_strs[5])).matrix();
    // robot
    robot = camera;
    // ptz
    ptz = Eigen::Vector2i(1000000, 1000000);
  }
  else {
    return false;
  }

  return true;
}

}

RtspClient::RtspClient(shared_ptr<ImageProcessor> image_processor) :
  image_processor_(image_processor)
{
}

RtspClient::~RtspClient()
{
  Stop();
}

int RtspClient::Connect(const string & rtsp_url) {
  int ret;
  videoindex=-1;
  ifmt_ctx = NULL;
  dec_ctx = NULL;


  LOG(INFO) << "connecting to " << rtsp_url << "...";
  avformat_network_init();

  AVCodec *codec = avcodec_find_decoder_by_name("h264");
  if (NULL == codec)
  {
    return -1;
  }
  
  dec_ctx = avcodec_alloc_context3(codec);
  if (NULL == dec_ctx)
  {
    return -1;

  }
  dec_ctx->time_base = AVRational{1, 1000};
  int result = avcodec_open2(dec_ctx, codec, NULL);
  if (result < 0) {
    return -1;
  }

//set option
	AVDictionary *format_opts = NULL;

	av_dict_set(&format_opts, "stimeout", std::to_string(10 * 1000000).c_str(), 0); //设置链接超时时间（us）
	av_dict_set(&format_opts, "rtsp_transport",  "tcp", 0); //设置推流的方式，默认udp
	av_dict_set(&format_opts, "buffer_size",  std::to_string(1920 * 1080 * 60).c_str(), 0);
	av_dict_set(&format_opts, "max_delay",  std::to_string(10 * 1000000).c_str(), 0);

//Input
  if ((ret = avformat_open_input(&ifmt_ctx, rtsp_url.c_str(), 0,  &format_opts)) < 0) {
    LOG(WARNING) <<  "Could not open rtsp server: " << rtsp_url << "!";
    return -1;

  }
  if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
    LOG(WARNING) <<  "Failed to retrieve input stream information";
    return -1;

  }
  for(int i=0; i<ifmt_ctx->nb_streams; i++) 
    if(ifmt_ctx->streams[i]->codecpar->codec_type==AVMEDIA_TYPE_VIDEO){
      videoindex = i;
      break;
    }

  av_dump_format(ifmt_ctx, 0, rtsp_url.c_str(), 0);

  LOG(INFO) << "connected to " << rtsp_url << ".";
  rtsp_url_ = rtsp_url;
  connected_ = true;
  return 0;
}

int RtspClient::Disconnect() {
  if (ifmt_ctx)
  {
    avformat_close_input(&ifmt_ctx);
    avformat_free_context(ifmt_ctx);
    ifmt_ctx = NULL;
  }
  if (dec_ctx)
  {
    avcodec_free_context(&dec_ctx);
    dec_ctx = NULL;
  }
}

void RtspClient::Run(){

  AVPacket* pkt = av_packet_alloc(); 
  AVFrame * frame = av_frame_alloc();  
  if (NULL == frame)
  {
    return;
  }
  PosedImage posed_image;
  stop_flag_ = false;
  
	while (!stop_flag_) {
		// Get an AVPacket
    // av_init_packet(&pkt);
    if (!connected_) {
      if (0 != Connect(rtsp_url_)) {
        std::this_thread::sleep_for(chrono::milliseconds( reconnect_seconds_  * 1000));
        continue;
      }
    }
		int ret = av_read_frame(ifmt_ctx, pkt);
		if (ret < 0) {
      connected_ = false;
      continue;
    }
		// ret = avcodec_decode_video2(dec_ctx, frame, &got_picture, &pkt);
    pkt->pts = av_rescale_q(pkt->pts, ifmt_ctx->streams[videoindex]->time_base, dec_ctx->time_base);
    ret = avcodec_send_packet(dec_ctx, pkt);

    while (ret >= 0) {
      ret = avcodec_receive_frame(dec_ctx, frame);
      if (ret == 0) {
        // AVFrameSideData * sei_data = NULL;
        AVFrameSideData * sei_data = av_frame_get_side_data(frame, AV_FRAME_DATA_SEI_UNREGISTERED);
        if (sei_data != NULL) {
          // char sei_content[1024];
          int sei_len = (sei_data->size - 16) + 1;
          char *sei_content = (char *)malloc(sei_len);
          memset(sei_content, 0x00, sei_len);
          memcpy(sei_content, sei_data->data + 16, sei_data->size - 16);
          sei_content[sei_data->size - 16] = '\0';
          ConvertSeiToPose(sei_content, posed_image.camera, posed_image.robot, posed_image.ptz);
          free(sei_content);

          const auto translation = posed_image.camera.translation();
          const auto robot_translation = posed_image.robot.translation();
          const auto ptz_angle = posed_image.ptz;
          if (translation[0] < 10000) {
            // LOG(INFO) << "time stamp: " << frame->pts << " sei content: " << std::string(sei_content);
            if (posed_image.yuv_img.empty()) {
                posed_image.yuv_img.resize(3);
                posed_image.yuv_img[0]  = Mat(frame->height,  frame->width, CV_16UC1);
                posed_image.yuv_img[1]  = Mat(frame->height,  frame->width, CV_16UC1);
                posed_image.yuv_img[2]  = Mat(frame->height,  frame->width, CV_16UC1);
            }
            ConvertFrameToYuvImg(frame, posed_image.yuv_img);
            image_processor_->SendPosedImage(posed_image);
          }

        }

      }
    // if (got_picture) {
    //   AVDictionaryEntry* side_data = av_dict_get(frame->metadata, "foo", NULL, 0);
    //   if (NULL != side_data)
    //   fprintf(stdout, "side_data is %s\n", side_data->value);
    // }
      av_frame_unref(frame);
    }

  }

  av_frame_free(&frame);
  av_packet_free(&pkt);

  LOG(INFO) << "finished  colorize_service.";
  return;
}


int RtspClient::Start(const string & rtsp_url, int reconnect_seconds) {
  reconnect_seconds_ = reconnect_seconds;
  int ret = Connect(rtsp_url);
  if (ret == 0) {
    work_thread_ = thread(bind(&RtspClient::Run, this));
    return 0;
  }

  if (reconnect_seconds <= 0) 
    return -1;

  connect_thread_ = thread([this, rtsp_url] () {
    stop_flag_ = false;
    while (!stop_flag_) {
      if (0 == Connect(rtsp_url)) {
        work_thread_ = thread(bind(&RtspClient::Run, this));
        break;
      }
      std::this_thread::sleep_for(chrono::milliseconds( reconnect_seconds_  * 1000));
    }
  });

  return 0;
}

void RtspClient::Stop() {
  stop_flag_ = true;
  if (connect_thread_.joinable())
    connect_thread_.join();
  if (work_thread_.joinable())
    work_thread_.join();

  Disconnect();

  return;
}


} // namespace colorize_service
