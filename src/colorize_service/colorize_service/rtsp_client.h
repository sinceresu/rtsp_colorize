#pragma once

#include <string>
#include <vector>
#include <memory>
#include <thread>

#include "gflags/gflags.h"
#include "glog/logging.h"
typedef struct AVFormatContext AVFormatContext;
typedef struct AVCodecContext AVCodecContext;
namespace colorize_service{
class ImageProcessor;
class RtspClient {
public:
  enum ERROR_CODE {
    SUCCEED = 0,
    FAILED_TO_CONNECTION = -1,
  };
  explicit RtspClient(std::shared_ptr<ImageProcessor> image_processor) ;

  virtual ~RtspClient();

  RtspClient(const RtspClient&) = delete;
  RtspClient& operator=(const RtspClient&) = delete;

  int Start(const std::string & rtsp_url, int reconnect_seconds = -1);
  void Stop();
private:
  int Connect(const std::string & rtsp_url);
  int Disconnect();
  void Run();

  int reconnect_seconds_;
  bool stop_flag_;
  std::thread work_thread_;
  std::thread connect_thread_;

  std::string rtsp_url_;
  bool connected_;

  std::shared_ptr<ImageProcessor> image_processor_;
  AVFormatContext *ifmt_ctx;
  AVCodecContext      *dec_ctx;
  int videoindex;

};

} // namespace colorize_service
