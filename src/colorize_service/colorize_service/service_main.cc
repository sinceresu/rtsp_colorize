#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "mqtt_bridge.h"
// #include "colorize_service.h"

using namespace std;
using namespace cv;

using namespace colorize_service;

DEFINE_string(streaming_url, "",
              "streaming url.");
DEFINE_string(
    calibration_filepath, "",
    "calibration filepath");

DEFINE_string(pcl_filepath, "",
              "pcl filepath");

DEFINE_string(ipc_ip, "",
              "ipc_ip");

DEFINE_string(ipc_port, "",
              "ipc_ip");

DEFINE_double(min_celcius, 0., "minimum temprature.");
DEFINE_double(max_celcius, 800., "maximum temprature.");

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if (ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}

int Run(int argc, char **argv)
{
  // LOG(INFO) << "start colorize service.";
  std::cout << "start colorize service.\r\n";

  /* test */
  /* ColorizeService colorize_service;
  colorize_service.SetParameters("1", FLAGS_pcl_filepath, FLAGS_calibration_filepath, FLAGS_min_celcius, FLAGS_max_celcius);
  if (0 != colorize_service.Start("1", FLAGS_streaming_url, FLAGS_ipc_ip, FLAGS_ipc_port) )
    return -1;
  while (1)
  {
    if (kbhit() != 0)
    {
      int ch = getchar();
      if (ch == 's')
      {
        std::cout << "s" << std::endl;
      }
      if (ch == 'x')
      {
        std::cout << "x" << std::endl;
        break;
      }
    }
  }
  colorize_service.Stop("1"); */

  MqttBridge mqtt_bridge;
  mqtt_bridge.Run();

  bool done = false;
  string input;
  while (!done)
  {
    std::cout << "Enter Command:\r\n";
    std::getline(std::cin, input);
    if (input == "help")
    {
      std::cout << "\nCommand List:\n"
                << "quit: Exit the program\n"
                << "save|id|flag: Save switch (id: camera id, flag: 0 close, 1 open)\n"
                << std::endl;
    }
    else if (input == "quit")
    {
      done = true;
    }
    else if (input.substr(0, 4) == "save")
    {
      std::vector<std::string> cmd_list;
      // SplitString(input, cmd_list, "|");
      utils_common_libs::SplitString(input, "|", cmd_list);
      if (cmd_list.size() >= 3)
      {
        mqtt_bridge.SaveImage(cmd_list[1], atoi(cmd_list[2].c_str()) == 1 ? true : false);
      }
      else
      {
        std::cout << "> Missing parameter Command" << std::endl;
      }
    }
    else
    {
      std::cout << "> Unrecognized Command" << std::endl;
    }
  }

  return 0;
}

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  Run(argc, argv);
}
