#include "Common.hpp"

namespace utils_common_libs
{
    static bool b_keep_running = false;

    void sig_handler(int sig)
    {
        if (sig == SIGINT)
        {
            b_keep_running = false;
            printf("Interrupt signal (\"%d\") received. \r\n", sig);
        }
    }

    void init()
    {
        b_keep_running = true;
        signal(SIGINT, sig_handler);
    }

    void shutdown()
    {
        b_keep_running = false;
    }

    bool is_ok()
    {
        return b_keep_running;
    }
}