#ifndef DateTime_HPP
#define DateTime_HPP

#pragma once
#include <iostream>
#include <string>
#include <chrono>

namespace utils_common_libs
{

    std::string GetCurrentDate(const std::string &_format = "%Y-%m-%d %H:%M:%S");

    time_t GetTimeStamp();

    time_t StrToTime_t(const std::string &_time, const std::string &_format = "%d-%d-%d %d:%d:%d");

}

#endif