#ifndef Utils_HPP
#define Utils_HPP

#pragma once
#include <time.h>
#include <chrono>
#include <unistd.h>
#include <thread>
#include <csignal>
#include <signal.h>

namespace utils_common_libs
{

    class CRate
    {
    public:
        CRate(float _hz = 1);
        ~CRate();

    private:
        float hz;
        float max_hz;
        bool status;
        long begin_time_stamp;
        long end_time_stamp;
        time_t get_time_stamp();

        static unsigned int load_counter;
        static bool b_exit_sig;
        static void sig_handler(int sig);

    public:
        bool is_ok();
        void sleep_wait();
    };

}

#endif
