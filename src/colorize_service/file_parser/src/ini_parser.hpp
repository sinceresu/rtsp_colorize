#ifndef INIParser_HPP
#define INIParser_HPP

#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <map>

namespace utils_common_libs
{

    std::string &TrimString(std::string &str);

    // INI文件结点存储结构
    class ININode
    {
    public:
        ININode(std::string root, std::string key, std::string value)
        {
            this->root = root;
            this->key = key;
            this->value = value;
        }
        std::string root;
        std::string key;
        std::string value;
    };

    // 键值对结构体
    class SubNode
    {
    public:
        void InsertElement(std::string key, std::string value)
        {
            sub_node.insert(std::pair<std::string, std::string>(key, value));
        }
        std::map<std::string, std::string> sub_node;
    };

    // INI文件操作类
    class CINIParser
    {
    public:
        CINIParser();
        ~CINIParser();

    private:
        std::string ini_file;                   // INI文件路径
        std::map<std::string, SubNode> map_ini; // INI文件内容的存储变量

    private:
        std::string &TrimString(std::string &str);                                                      // 去除空格
        int ReadINI(std::string path);                                                                  // 读取INI文件
        std::vector<ININode>::size_type GetSize() { return map_ini.size(); }                            // 获取INI文件的结点数
        std::string GetValue(std::string root, std::string key, std::string defaultValue);              // 由根结点和键获取值
        std::vector<ININode>::size_type SetValue(std::string root, std::string key, std::string value); // 设置根结点和键获取值
        void Travel();                                                                                  // 遍历打印INI文件
        int WriteINI(std::string path);                                                                 // 写入INI文件
        void Clear() { map_ini.clear(); }                                                               // 清空

    public:
        void LoadFIle(std::string file);
        std::string GetData(std::string root, std::string key, std::string defaultValue = "");
        void SetData(std::string root, std::string key, std::string value);
    };

}

#endif