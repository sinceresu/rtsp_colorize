set(MODULE_NAME "http_com_7.x")

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib/${MODULE_NAME})
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib/${MODULE_NAME})

# set(CMAKE_DEBUG_POSTFIX "_d")
#
# set(CMAKE_RELEASE_POSTFIX "_r")

set(CMAKE_BUILD_TYPE DEBUG)

# ##############################################################################
# Build ##
# ##############################################################################

aux_source_directory(./src http_com_DIR_LIB_SRCS)

if(BUILD_SHARED_LIBS)
  # Declare a C++ library
  add_library(${MODULE_NAME}_shared SHARED ${http_com_DIR_LIB_SRCS})

  # Specify libraries to link a library or executable target against
  target_link_libraries(${MODULE_NAME}_shared crypto ssl)

  # set_target_properties(${MODULE_NAME}_shared PROPERTIES DEBUG_POSTFIX "_d")
  #
  # set_target_properties(${MODULE_NAME}_shared PROPERTIES RELEASE_POSTFIX "_r")

  set_target_properties(${MODULE_NAME}_shared PROPERTIES OUTPUT_NAME
                                                         ${MODULE_NAME})
endif()

if(BUILD_STATIC_LIBS)
  # Declare a C++ library
  add_library(${MODULE_NAME}_static STATIC ${http_com_DIR_LIB_SRCS})

  # Specify libraries to link a library or executable target against
  target_link_libraries(${MODULE_NAME}_static crypto ssl)

  # set_target_properties(${MODULE_NAME}_static PROPERTIES DEBUG_POSTFIX "_d")
  #
  # set_target_properties(${MODULE_NAME}_static PROPERTIES RELEASE_POSTFIX "_r")

  set_target_properties(${MODULE_NAME}_static PROPERTIES OUTPUT_NAME
                                                         ${MODULE_NAME})
endif()

# file(GLOB PUBLIC_HEADERS src/*.hpp)
#
set(PUBLIC_HEADERS src/http_client.hpp)
install(FILES ${PUBLIC_HEADERS}
        DESTINATION ${PROJECT_SOURCE_DIR}/include/${MODULE_NAME})
