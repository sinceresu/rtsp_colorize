#ifndef httpClient_HPP
#define httpClient_HPP

#pragma once
#include <string>
#include <functional>
#include <boost/format.hpp>
#include "mongoose.h"

namespace utils_common_libs
{

    // typedef void (*DataCallback)(int code, const char *response, int lenght, void *user_data);
    typedef std::function<void(int code, const char *response, int lenght, void *user_data)> DataCallback;

    class CHttpClient
    {
    public:
        CHttpClient();
        ~CHttpClient();

    private:
        static void ev_handler(struct mg_connection *c, int ev, void *ev_data, void *fn_data);
        void request(const char *type, const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data = nullptr);
        static void response(int code, const char *response, int lenght, void *fn_data);

    public:
        void get(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data = nullptr);
        void post(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data = nullptr);
        void put(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data = nullptr);
    };

    class CHttpRequest
    {
    public:
        CHttpRequest();
        ~CHttpRequest();

    public:
        static int get(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response);
        static int post(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response);
        static int put(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response);
    };

}

#endif