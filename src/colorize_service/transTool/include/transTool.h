
#include <map>
#include <string>
#include <functional>

#include <ecal/ecal.h>
#include <ecal/ecal_publisher.h>
#include <ecal/ecal_subscriber.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <map>
#include <atomic>
#include <thread>

namespace transTool
{
    struct subPtr
    {
        eCAL::CSubscriber* _pSub;
        std::thread* _pthread;
        bool _threadBreaker;
        std::string _topicName;
        std::function<void(const char* topic_name_, const struct eCAL::SReceiveCallbackData* data_)> _func;
        subPtr()
        {
            _pSub = NULL;
            std::thread* _pthread = NULL;
            _threadBreaker = true;
        }
        ~subPtr()
        {
            destroy();
        }

        void destroy()
        {
            _threadBreaker = false;
            _pthread->join();
            _pSub->Destroy();
            delete _pthread;
            delete _pSub;
        }

    };
    struct pubPtr
    {
        eCAL::CPublisher* _pPub;
        std::string _topicName;
        pubPtr()
        {
            _pPub = NULL;
        }
        ~pubPtr()
        {
            destroy();
        }
        void destroy()
        {
            _pPub->Destroy();
            delete _pPub;
        }
    };

    class transTool {

    public:
        transTool(std::string name);
        ~transTool();

        void publisher(std::string topicName,std::string msg);
        void subscriber(std::string topicName,std::function<void(const char* topic_name_, const struct eCAL::SReceiveCallbackData* data_)> func);

    private:
        void init(std::string name);
        void destroy();

        std::map<std::string,subPtr*> _mapOfSubscriber;
        std::map<std::string,pubPtr*> _mapOfPublisher;
    };
}
