#include "colorizer.h"
#include<algorithm>

#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/frustum_culling.h>

#include <pcl/filters/frustum_culling.h>

#include "err_code.h"

using namespace std;
using namespace cv;

namespace pcl_colorize{
namespace {
constexpr float kHorizontalFOV = 40.0f;
constexpr float kVerticalFOV = 60.0f;
constexpr float kNearPlaneDistance = 1.3f;
constexpr float kFarPlaneDistance = 30.0f;
constexpr float kPlaneDistance = 0.2f;
constexpr float kOcculudeDistance = 0.2f;
constexpr int kErodeSize = 6;

void FilterImage(const Mat & input_img, Mat & filtered_img) {
  filtered_img = input_img.clone();
  for (int row = 1; row < input_img.rows - 1; ++row){
    for (int col = 1; col < input_img.cols - 1; ++col){
      cv::Vec4f pixel_value =  input_img.at<cv::Vec4f>(row, col);
      vector<float> pixels_nearby ;
      cv::Vec4f pixel_top =  input_img.at<cv::Vec4f>(row - 1, col);
      cv::Vec4f pixel_right =  input_img.at<cv::Vec4f>(row, col + 1);
      cv::Vec4f pixel_bottom =  input_img.at<cv::Vec4f>(row + 1, col);
      cv::Vec4f pixel_left =  input_img.at<cv::Vec4f>(row, col - 1);
      
      pixels_nearby.push_back( cv::norm(Vec3f(pixel_top[1], pixel_top[2], pixel_top[3])));
      pixels_nearby.push_back(cv::norm(Vec3f(pixel_right[1], pixel_right[2], pixel_right[3])));
      pixels_nearby.push_back(cv::norm(Vec3f(pixel_bottom[1], pixel_bottom[2], pixel_bottom[3])));
      pixels_nearby.push_back(cv::norm(Vec3f(pixel_left[1], pixel_left[2], pixel_left[3])));
      std::sort(pixels_nearby.begin(), pixels_nearby.end()) ;
      if (pixels_nearby[3] - pixels_nearby[0] < 0.2 && sqrt(pixel_value[1] * pixel_value[1] +  pixel_value[2] * pixel_value[2] +  pixel_value[3] * pixel_value[3]) - pixels_nearby[3] > 0.2 ) {
        cv::Vec4f& temp_3d = filtered_img.at<cv::Vec4f>(row, col) ;
        temp_3d = cv::Vec4f(temp_3d[0], numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
      }

    }
  }  
}
void FilterRangeImage(const Mat & range_img, Mat & index_img) {
  for (int row = 1; row < range_img.rows - 1; ++row){
    for (int col = 1; col < range_img.cols - 1; ++col){
      float pixel_range =  range_img.at<float>(row, col);
      if (index_img.at<int32_t>(row, col) == -1)
        continue;
      vector<float> pixels_nearby ;
      float pixel_top =  range_img.at<float>(row - 1, col);
      float pixel_right =  range_img.at<float>(row, col + 1);
      float pixel_bottom =  range_img.at<float>(row + 1, col);
      float pixel_left =  range_img.at<float>(row, col - 1);
      if (pixel_top > 10000 || pixel_right > 10000 || pixel_bottom > 10000 || pixel_left > 10000)
        continue;

      pixels_nearby.push_back( pixel_top);
      pixels_nearby.push_back(pixel_right);
      pixels_nearby.push_back(pixel_bottom);
      pixels_nearby.push_back(pixel_left);
      std::sort(pixels_nearby.begin(), pixels_nearby.end()) ;
      if (pixels_nearby[3] - pixels_nearby[0] < kPlaneDistance && pixel_range - pixels_nearby[3] > kOcculudeDistance ) {
        index_img.at<int32_t>(row, col) =  - 1 ; //invalidate
      }
    }
  }  
}
void FilterRangeImage1(const Mat & range_img, Mat & index_img) {
    int erode_size  = kErodeSize;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                       cv::Size( 2*erode_size + 1, 2*erode_size+1 ) );
    cv::Mat eroded = cv::Mat();
    cv::erode(range_img, eroded, element);
  int dilation_size  = kErodeSize;
  element = cv::getStructuringElement( cv::MORPH_RECT,
                       cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ) );

    cv::Mat dilated = cv::Mat();
    cv::dilate(eroded, dilated, element);

  for (int row = 0; row < range_img.rows; ++row){
    for (int col = 0; col < range_img.cols; ++col){
      float pixel_range =  range_img.at<float>(row, col);
      if (pixel_range > 10000)
        continue;

      float filtered_range =  dilated.at<float>(row, col);
      // if (row == 231 && col == 212) {
      //   LOG(INFO) << pixel_range;
      //   for (int y = row - 9; y <= row + 9; ++y){
      //     for (int x = col - 9; x <= col + 9; ++x){
      //         std::cout << dilated.at<float>(y, x ) << " ";

      //     }
      //     std::cout <<endl;
      //   }
      // }
      if ( pixel_range - filtered_range > kOcculudeDistance ) {
        index_img.at<int32_t>(row, col) =  - 1 ; //invalidate
      }

    }
  }  
}

}

Colorizer::Colorizer() :
        horizontal_fov_(kHorizontalFOV),
        vertical_fov_(kVerticalFOV),
        near_plane_distance_(kNearPlaneDistance),
        far_plane_distance_(kFarPlaneDistance)
{
  // num_threads_ = omp_get_max_threads();
// Note: This assumes ros coordinate system where X is forward, Y is left, and Z is up. convert to the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  ros_to_cam_ << 0, -1, 0, 0,
                                    0, 0, -1, 0,
                                    1, 0, 0, 0,
                                    0, 0, 0, 1;
}

Colorizer::~Colorizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}

int Colorizer::SetParamters(
                          float near_plane_distance,
                          float far_plane_distance) {

    near_plane_distance_ = near_plane_distance;
    far_plane_distance_ = far_plane_distance;
    return ERRCODE_OK;
}


int Colorizer::LoadCalibrationFile(const std::string& calibration_filepath) {
	cv::FileStorage fs;

  fs.open(calibration_filepath, cv::FileStorage::READ);
  if(!fs.isOpened()){
    LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
    return ERRCODE_FAILED;
  }
    
  fs["IRCameraMat"] >> intrinsic_mat_;
  fs["IRDistCoeff"] >> distortion_coeffs_;
  fs["IRImageSize"] >> image_size_;
  CalculateFov(image_size_);
  return ERRCODE_OK;

 }
 
pcl::PointCloud<pcl::PointXYZ>::Ptr Colorizer::CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) {

  pcl::FrustumCulling<pcl::PointXYZ> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_ + 1.0f);
  fc_.setHorizontalFOV (horizontal_fov_  + 1.0f);
  fc_.setNearPlaneDistance (near_plane_distance_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camera_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices); 

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  inliers->indices = move(filter_indices);
  pcl::ExtractIndices<pcl::PointXYZ> extract;
  extract.setInputCloud (point_cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_filtered);
  return cloud_filtered;
}

int Colorizer::Initialize(const cv::Size& image_size) {
  CalculateFov(image_size);
  Eigen::Matrix4f robot2cam;
// Note: This assumes a coordinate system where X is forward, Y is up, and Z is right. To convert from the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  robot2cam << 0, 0, 1, 0,
            0,-1, 0, 0,
            1, 0, 0, 0,
            0, 0, 0, 1;
  camara_to_ref_for_culling_ = robot2cam;  
  initialized_ = true;
  return ERRCODE_OK;
}

void Colorizer::CalculateFov(const cv::Size& image_size) {
  horizontal_fov_ = 2 * atan2(image_size.width/2, intrinsic_mat_.at<double>(0, 0)) * 180 / M_PI;
  vertical_fov_ = 2 * atan2(image_size.height/2, intrinsic_mat_.at<double>(1, 1))* 180 / M_PI;
}

int Colorizer::Colorize( const cv::Mat& image, const Eigen::Matrix4f &camera_pose, cv::Mat& image_3d) {
  if (!raw_pcl_) {
    LOG(ERROR) << "Raw point cloud is not set!";
    return ERRCODE_NOT_INITIALIZED;
  }
  if (intrinsic_mat_.empty()) {
    LOG(ERROR) << "Calibration file is not set!";
    return ERRCODE_NOT_INITIALIZED;
  }

  if(!initialized_) {
    int result = Initialize(cv::Size(image.cols, image.rows));
    if (result != ERRCODE_OK) {
      return result;
    }
  }
    // LOG(INFO)   << "UpdataRangeImages: " ;

  Eigen::Matrix4f world_to_camera_transform = camera_pose.inverse();

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);
  // LOG(INFO) <<  target_to_camera ;

  Eigen::Matrix4f camera_to_world_for_culling = camera_pose * camara_to_ref_for_culling_;
  auto culled_pcl = CullingPointCloud(raw_pcl_, camera_to_world_for_culling);
  if (culled_pcl->empty()){
    LOG(WARNING) << "Culling output no point!";
    return ERRCODE_FAILED;
  }

  std::vector<cv::Point3f> points_to_colorize;
  points_to_colorize.reserve(culled_pcl->size());
  for (size_t i = 0; i < culled_pcl->size(); ++i) {
      const auto& point = culled_pcl->at(i);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
  }
    
  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  cv::Mat rotation_vec_t = rotation_vec.t();
  cv::Mat transition_vec_t = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;

  cv::projectPoints(points_to_colorize, rotation_vec_t, transition_vec_t, intrinsic_mat_, distortion_coeffs_, image_points);
  std::vector<cv::Point3f> points_to_camera;
  cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);

  image_3d = cv::Mat(image.rows, image.cols, CV_32FC4, cv::Vec4f (numeric_limits<float>::max(), 0, 0, 0));
  cv::Mat range_image = cv::Mat(image.rows, image.cols, CV_32F, numeric_limits<float>::max());
  cv::Mat index_image = cv::Mat(image.rows, image.cols, CV_32S, -1);

// #pragma omp parallel for num_threads(num_threads_) schedule(guided, 8)
  for (size_t i = 0; i < image_points.size(); ++i)
  {
    float y = image_points[i].y, x = image_points[i].x;
    int col = (int)x, row = (int)y;
    if (row >= 0 && row < image.rows && col >= 0 && col < image.cols) {
      float range = cv::norm(points_to_camera[i]);
      //only keep  point  nearest to  the pixel 
      if (range > range_image.at<float>(row, col))
          continue;
      range_image.at<float>(row, col) = range;
      index_image.at<int32_t>(row, col) = i;
    }
  }

  FilterRangeImage1(range_image, index_image);

  for (int row = 0; row < index_image.rows; ++row){
    for (int col = 0; col < index_image.cols; ++col){
      const int32_t & point_index = index_image.at<int32_t>(row, col);
      if (point_index >= 0 ) {
        const auto &raw_point  = culled_pcl->at(point_index);
        cv::Vec4f temp_3d(image.at<float>(row, col), raw_point.x, raw_point.y, raw_point.z);
        // if ((Eigen::Vector3f(raw_point.x, raw_point.y, raw_point.z) - Eigen::Vector3f(-0.524900, 8.403600, 67.176003)).norm() < 0.001) {
        //   LOG(INFO) <<  range_image.at<float>(row, col) ;
        // }
        // if ((Eigen::Vector3f(raw_point.x, raw_point.y, raw_point.z) - Eigen::Vector3f(-0.095900, 8.554800, 67.160103)).norm() < 0.001) {
        //   LOG(INFO) <<  range_image.at<float>(row, col) ;
        // }
        
        image_3d.at<cv::Vec4f>(row, col) = temp_3d;
      } else {
        cv::Vec4f temp_3d(image.at<float>(row, col), numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
        image_3d.at<cv::Vec4f>(row, col) = temp_3d;
      }
    }
  }
  return ERRCODE_OK;
}

int Colorizer::GetCameraFov(double &horizontal_fov, double& vertical_fov) {
  if (intrinsic_mat_.empty()) {
    return ERRCODE_NOT_INITIALIZED;
  }
  horizontal_fov = horizontal_fov_;
  vertical_fov = vertical_fov_;
  return ERRCODE_OK;
}

int Colorizer::GetCameraSize(cv::Size &size) {
  size = image_size_;
  return ERRCODE_OK;
}

} // namespace pcl_colorize
