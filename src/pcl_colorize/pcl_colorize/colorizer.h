#ifndef _PCL_COLORIZE_COLORIZER_H
#define _PCL_COLORIZE_COLORIZER_H
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>  
#include <pcl/point_types.h>

#include "common.h"


namespace pcl_colorize{
namespace {
  typedef pcl::PointXYZRGB PointT;
  typedef pcl::PointCloud<PointT> PointCloud;
}

class Colorizer 
{
public:
  explicit Colorizer() ;

  virtual ~Colorizer();

  Colorizer(const Colorizer&) = delete;
  Colorizer& operator=(const Colorizer&) = delete;

   int LoadCalibrationFile(const std::string& calibration_filepath);
  int SetParamters(
                          float near_plane_distance,
                          float far_plane_distance) ;
  void SetRawPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& raw_pcl) {
    raw_pcl_.reset(new pcl::PointCloud<pcl::PointXYZ>(*raw_pcl) );
  };

  virtual int Colorize(const cv::Mat& image, const Eigen::Matrix4f &ref_pose, cv::Mat& image_3d) ;
  virtual int GetCameraFov(double &horizontal_fov, double& vertical_fov) ;
  virtual int GetCameraSize(cv::Size &size);

private:
  virtual int Initialize(const cv::Size& image_size);

  cv::Mat GenerateRangeImage(const cv::Mat& image, const std::vector<cv::Point2f>&image_points, const  std::vector<cv::Point3f>& points_to_camera);
  void CalculateFov(const cv::Size& image_size);
  // std::vector<int> CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) ;
  pcl::PointCloud<pcl::PointXYZ>::Ptr CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) ;
  // pcl::PointCloud<pcl::PointXYZ>::Ptr ExtractCulledPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) ;
  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;

  float horizontal_fov_;
  float vertical_fov_;

  float near_plane_distance_; 
  float far_plane_distance_;


  int num_threads_;
   pcl::PointCloud<pcl::PointXYZ>::Ptr raw_pcl_;
   
   std::string calibration_filepath_;
   bool initialized_ = false;

  Eigen::Matrix4f camara_to_ref_for_culling_;

  Eigen::Matrix4f ros_to_cam_;

 };

} // namespace pcl_colorize
#endif  
