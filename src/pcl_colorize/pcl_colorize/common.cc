

#include "opencv2/opencv.hpp"


namespace pcl_colorize{


cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt)
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<cv::Vec3b>(0,0);
}

} // namespace pcl_colorize
