#ifndef _PCL_COLORIZE_TIMED_IMAGE_H
#define _PCL_COLORIZE_TIMED_IMAGE_H

#include "opencv2/opencv.hpp"

namespace pcl_colorize{
cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt);
} // namespace pcl_colorize

#endif  