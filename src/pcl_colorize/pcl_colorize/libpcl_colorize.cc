// #include "pcl_colorize/libpcl_colorize.h"
#include "libpcl_colorize.h"
#include "pcl_colorizer.h"

namespace pcl_colorize{

std::shared_ptr<PclColorizeInterface> CreatePclColorizer()
{
    return std::shared_ptr<PclColorizeInterface>(new PclColorizer());
}

} // namespace pcl_colorize
