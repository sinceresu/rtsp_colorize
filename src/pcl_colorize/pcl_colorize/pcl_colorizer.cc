#include "pcl_colorizer.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <cstdlib> 
#include <boost/filesystem.hpp>


#include <opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include  "colorizer.h"
#include "common.h"

#include "err_code.h"

// #define OUTPUT_PCL_FILES

using namespace std;

namespace pcl_colorize{

namespace {
}



PclColorizer::PclColorizer()
: stop_flag_(false)
{
  colorizer = make_shared<Colorizer>();
}

int PclColorizer::SetCalibrationFile(const std::string & calibration_filepath) {
  return colorizer->LoadCalibrationFile(calibration_filepath);
}

int  PclColorizer::SetRawPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & pointcloud) {
  if (pointcloud->empty()) {
    LOG(WARNING) << "Intput point cloud is empty: ";
    return ERRCODE_FAILED;
  } 
  colorizer->SetRawPointCloud(pointcloud);
  return ERRCODE_OK;
}

int  PclColorizer::Colorize(const cv::Mat& image, const Eigen::Affine3f &cam_pose, cv::Mat& temp_img_3d) {
  return colorizer->Colorize(image, cam_pose.matrix(), temp_img_3d);
 }

 int PclColorizer::GetCameraFov(double &horizontal_fov, double& vertical_fov) {
  return colorizer->GetCameraFov(horizontal_fov, vertical_fov);
 }

  int PclColorizer::GetCameraSize(cv::Size &size) {
    return colorizer->GetCameraSize(size);
  }

} // namespace pcl_colorize
