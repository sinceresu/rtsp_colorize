#ifndef _PCL_COLORIZE_PCL_COLORIZE_H
#define _PCL_COLORIZE_PCL_COLORIZE_H

#include <vector>
#include <memory>

// #include "pcl_colorize/pcl_colorize_interface.h"
#include "pcl_colorize_interface.h"

namespace pcl_colorize{
class Colorizer; 

class PclColorizer : public PclColorizeInterface
{
public:
  PclColorizer();
  ~PclColorizer(){};
  virtual int SetCalibrationFile(const std::string & calibration_filepath) override;
  virtual int  SetRawPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & pointcloud) override;
  virtual int  Colorize(const cv::Mat& image, const Eigen::Affine3f &cam_pose, cv::Mat& temp_img_3d) override;
  virtual int GetCameraFov(double &horizontal_fov, double& vertical_fov) override;
  virtual int GetCameraSize(cv::Size &size) override;
  // virtual  int  ColorizeVideoFrames(std::vector<std::vector<std::string>>&video_filenames, const std::vector<Eigen::Matrix4f> & track_poses, , const std::vector<double> &  offset_secs, const std::string & save_directory,  const std::string& map_name) override ;
private:

  float vertical_fov ;
  float occlude_distance;

  std::shared_ptr<Colorizer>  colorizer ;

  pcl::PointCloud<pcl::PointXYZ>::Ptr map_pcl_;
  bool stop_flag_;

  bool initialized_ = false;
};
} // namespace pcl_colorize

#endif  
