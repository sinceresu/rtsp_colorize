#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include "pcl_colorize_interface.h"
#include "libpcl_colorize.h"

DEFINE_string(
    calibration_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              

DEFINE_string(
    image_filepath, "",
"Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(
    pcl_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(
    pose_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

using namespace std;
using namespace Eigen;

namespace pcl_colorize{

namespace {
Matrix4f readMatrix(const char *filename)
    {
    int cols = 0, rows = 0;
    double buff[2000];

    // Read numbers from file into buffer.
    ifstream infile;
    infile.open(filename);
    while (! infile.eof())
        {
        string line;
        getline(infile, line);

        int temp_cols = 0;
        stringstream stream(line);
        while(! stream.eof())
            stream >> buff[cols*rows+temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
        }

    infile.close();

    // rows--;

    // Populate matrix with numbers.
    Matrix4f result;
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            result(i,j) = buff[ cols*i+j ];
    
    return result;
}

}

void ConvertDepthToPcl(const cv::Mat& image, const cv::Mat& jet_image, pcl::PointCloud<pcl::PointXYZRGB>::Ptr & output) {
  for (int row = 0; row < image.rows; ++row){
    for (int col = 0; col < image.cols; ++col){
      const cv::Vec4f & pixel = image.at<cv::Vec4f>(row, col);
      const cv::Vec3b & bgr_pixel = jet_image.at<cv::Vec3b>(row, col);
      // if (row == 90 && col == 45 ) {
      //     LOG(INFO) << "rgb." << (int)bgr_pixel[2]  << ", " << (int)bgr_pixel[1]  << ", " << (int)bgr_pixel[0] ;
      //     LOG(INFO) << "xyz." <<pixel[1]  << ", " <<pixel[2]  << ", " <<pixel[3] ;
      // }
      if (pixel[1] > 10000) {
        continue;
      }

      pcl::PointXYZRGB point(bgr_pixel[2], bgr_pixel[1], bgr_pixel[0]);
      point.x = pixel[1];
      point.y = pixel[2];
      point.z = pixel[3];

      output->push_back(point);
    }
  }
}
void Test(int argc, char** argv) {
    auto pcl_colorizer = CreatePclColorizer();
    pcl_colorizer->SetCalibrationFile(FLAGS_calibration_filepath);
    pcl::PointCloud<pcl::PointXYZ>::Ptr raw_map(new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::io::loadPCDFile<pcl::PointXYZ>(FLAGS_pcl_filepath, *raw_map);

    pcl_colorizer->SetRawPointCloud(raw_map);
    cv::Mat image = cv::imread(FLAGS_image_filepath);

    ifstream pose_file(FLAGS_pose_filepath);
    Eigen::Matrix4f pose_mat = readMatrix(FLAGS_pose_filepath.c_str());
    LOG(INFO) << endl << pose_mat;

    Eigen::Affine3f pose( pose_mat.cast<float>());
    cv::Mat colored_image;

    pcl_colorizer->Colorize(image, pose, colored_image);

      pcl::PointCloud<pcl::PointXYZRGB>::Ptr  output_pcl(new (pcl::PointCloud<pcl::PointXYZRGB>));

    ConvertDepthToPcl(colored_image, image, output_pcl);
    pcl::io::savePCDFileBinary("/home/sujin/output/colorized.pcd", *output_pcl);
}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;


  pcl_colorize::Test(argc, argv);

}
