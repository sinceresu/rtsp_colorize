#include <algorithm>
#include <fstream>
#include <iostream>
#include <chrono>

#include "ros/ros.h"
#include "rosbag/view.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <tf2_msgs/TFMessage.h>
#include "tf2_eigen/tf2_eigen.h"

#include <Eigen/Geometry>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"
#include "pcl_colorize_interface.h"


#include "libpcl_colorize.h"

#include "undistort_service_msgs/PosedImage.h"

using namespace std;
using namespace cv;

DEFINE_string(
    calibration_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              

DEFINE_string(bag_filepath, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(pcl_filepath, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
namespace pcl_colorize {

void TempToJet(const cv::Mat &temp, float celsius_min, float celsius_max,
                               cv::Mat &color) {

  const double alpha = 255.0 / (celsius_max - celsius_min);
  const double beta = -alpha * celsius_min;

  temp.convertTo(color, CV_8UC1, alpha, beta);
  cv::applyColorMap(color, color, cv::COLORMAP_JET);
}
void ConvertDepthToPcl(const cv::Mat& image, pcl::PointCloud<pcl::PointXYZI>::Ptr & output) {
  for (int row = 0; row < image.rows; ++row){
    for (int col = 0; col < image.cols; ++col){
      const cv::Vec4f & pixel = image.at<cv::Vec4f>(row, col);
      if (pixel[0] > numeric_limits<float>::max() - 1) {
        continue;
      }
      pcl::PointXYZI point(pixel[0]);
      point.x = pixel[1];
      point.y = pixel[2];
      point.z = pixel[3];

      output->push_back(point);
    }
  }
}
int Run(int argc, char** argv) {
  LOG(INFO) << "start test service.";
  auto pcl_colorizer = pcl_colorize::CreatePclColorizer();
  pcl_colorizer->SetCalibrationFile(FLAGS_calibration_filepath);
  pcl::PointCloud<pcl::PointXYZ>::Ptr raw_map(new pcl::PointCloud<pcl::PointXYZ>() );
  pcl::io::loadPLYFile<pcl::PointXYZ>(FLAGS_pcl_filepath, *raw_map);
  pcl_colorizer->SetRawPointCloud(raw_map);

  rosbag::Bag bag;
  try {
      bag.open(FLAGS_bag_filepath, rosbag::bagmode::Read);
  }
  catch (const rosbag::BagException& ex) {
    LOG(ERROR) << "Error Reading Bag: " <<  ex.what();
    return -1;
  }

  rosbag::View view(bag);
  const ::ros::Time begin_time = view.getBeginTime();
  const double duration_in_seconds =
        (view.getEndTime() - begin_time).toSec();
  cv::Mat temp_img;

  for (const rosbag::MessageInstance& message : view) {
    if (!::ros::ok()) {
      break;
    }
    if(message.isType<sensor_msgs::Image>()) {
      auto img_msg = message.instantiate<sensor_msgs::Image>();
      cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);
      temp_img = cv_ptr->image.clone();
    }
    if(message.isType<tf2_msgs::TFMessage>()) {
      auto transform_msg = message.instantiate<tf2_msgs::TFMessage>();
      Eigen::Affine3f robot_to_map =  tf2::transformToEigen (transform_msg->transforms[0]).cast<float>();
      if (!temp_img.empty()) {
        LOG(INFO) <<  "pose: " << std::endl <<  robot_to_map.matrix() ;
        cv::Mat jet_image;
        TempToJet(temp_img, 15, 30, jet_image);
        cv::imwrite("/home/sujin/output/jet.png", jet_image);
        pcl::PointCloud<pcl::PointXYZI>::Ptr  output_pcl(new (pcl::PointCloud<pcl::PointXYZI>));
        pcl_colorizer->Colorize(temp_img, robot_to_map, output_pcl);
        // pcl::io::savePCDFileBinary("/home/sujin/output/output.pcd", *output_pcl); 
        break;
      }
    }
    if(message.isType<undistort_service_msgs::PosedImage>()) {
      auto posed_img_msg = message.instantiate<undistort_service_msgs::PosedImage>();
      cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(posed_img_msg->image, sensor_msgs::image_encodings::TYPE_32FC1);
      temp_img = cv_ptr->image.clone();

      cv::Mat jet_image;
      TempToJet(temp_img, 15, 30, jet_image);
      cv::imwrite("/home/sujin/output/jet_output.png", jet_image);

      Eigen::Affine3d robot_to_map;
      Eigen::fromMsg(posed_img_msg->pose, robot_to_map);

      LOG(INFO) <<  "pose: " << std::endl <<  robot_to_map.matrix() ;
      cv::Mat colored_image;
      pcl_colorizer->Colorize(temp_img, robot_to_map, colored_image);
      pcl::PointCloud<pcl::PointXYZI>::Ptr  output_pcl(new (pcl::PointCloud<pcl::PointXYZI>));
      ConvertDepthToPcl(colored_image, output_pcl);
      pcl::io::savePCDFileBinary("/home/sujin/output/final_output.pcd", *output_pcl);
      break;
    }      
  }
}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ::ros::init(argc, argv, " test rgbd");
  
  pcl_colorize::Run(argc, argv);

}


