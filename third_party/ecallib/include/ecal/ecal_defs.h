#ifndef ecal_defs_h_included
#define ecal_defs_h_included
#define ECAL_VERSION_MAJOR (5)
#define ECAL_VERSION_MINOR (10)
#define ECAL_VERSION_PATCH (0)
#define ECAL_VERSION "v5.10.0-nightly-86-g596388fe-dirty"
#define ECAL_DATE "23.02.2022"
#define ECAL_PLATFORMTOOLSET ""

#define ECAL_INSTALL_APP_DIR     "bin"
#define ECAL_INSTALL_SAMPLES_DIR "bin"
#define ECAL_INSTALL_LIB_DIR     "lib"
#define ECAL_INSTALL_CONFIG_DIR  "etc/ecal"
#define ECAL_INSTALL_INCLUDE_DIR "include"
#define ECAL_INSTALL_PREFIX      "/home/test/lib/ecal/output"

#endif // ecal_defs_h_included
