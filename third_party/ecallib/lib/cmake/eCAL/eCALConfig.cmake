# ========================= eCAL LICENSE =================================
#
# Copyright (C) 2016 - 2019 Continental Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ========================= eCAL LICENSE =================================


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was eCALConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../../../" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

macro(check_required_components _NAME)
  foreach(comp ${${_NAME}_FIND_COMPONENTS})
    if(NOT ${_NAME}_${comp}_FOUND)
      if(${_NAME}_FIND_REQUIRED_${comp})
        set(${_NAME}_FOUND FALSE)
      endif()
    endif()
  endforeach()
endmacro()

####################################################################################

# Set some build relevant variables to build samples appropriately
set(eCAL_install_samples_dir "bin")

set(eCAL_VERSION_MAJOR  5)
set(eCAL_VERSION_MINOR  10)
set(eCAL_VERSION_PATCH  0)
set(eCAL_VERSION_STRING 5.10.0)

# eCAL is provided only with Release and Debug Version, thus map the other configs to Release build.
set(CMAKE_MAP_IMPORTED_CONFIG_MINSIZEREL Release "")
set(CMAKE_MAP_IMPORTED_CONFIG_RELWITHDEBINFO Release "")

find_package(Protobuf REQUIRED)

include("${PACKAGE_PREFIX_DIR}/lib/cmake/eCAL/helper_functions/ecal_add_functions.cmake")
include("${PACKAGE_PREFIX_DIR}/lib/cmake/eCAL/helper_functions/ecal_helper_functions.cmake")
include("${PACKAGE_PREFIX_DIR}/lib/cmake/eCAL/helper_functions/ecal_install_functions.cmake")
include("${PACKAGE_PREFIX_DIR}/lib/cmake/eCAL/eCALTargets.cmake")

# careful, this is a Windows hack to find protobuf etc. with the eCAL installation
# To be removed if there is a better solution like Conan e.g.
#if(WIN32)
#  list(APPEND CMAKE_MODULE_PATH "${PACKAGE_PREFIX_DIR}/../../../../cmake")
#  list(APPEND CMAKE_PREFIX_PATH "${PACKAGE_PREFIX_DIR}/../../../../cmake")
#endif()

find_package(CMakeFunctions REQUIRED)

find_package(Threads REQUIRED)
