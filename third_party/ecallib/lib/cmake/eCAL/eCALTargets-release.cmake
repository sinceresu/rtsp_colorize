#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "eCAL::proto" for configuration "Release"
set_property(TARGET eCAL::proto APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::proto PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_proto.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::proto )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::proto "${_IMPORT_PREFIX}/lib/libecal_proto.a" )

# Import target "eCAL::pb" for configuration "Release"
set_property(TARGET eCAL::pb APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::pb PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_pb.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::pb )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::pb "${_IMPORT_PREFIX}/lib/libecal_pb.a" )

# Import target "eCAL::core_c" for configuration "Release"
set_property(TARGET eCAL::core_c APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::core_c PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_core_c.so.5.10.0"
  IMPORTED_SONAME_RELEASE "libecal_core_c.so.5"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::core_c )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::core_c "${_IMPORT_PREFIX}/lib/libecal_core_c.so.5.10.0" )

# Import target "eCAL::core" for configuration "Release"
set_property(TARGET eCAL::core APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::core PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_core.so.5.10.0"
  IMPORTED_SONAME_RELEASE "libecal_core.so.5"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::core )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::core "${_IMPORT_PREFIX}/lib/libecal_core.so.5.10.0" )

# Import target "eCAL::utils" for configuration "Release"
set_property(TARGET eCAL::utils APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::utils PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_utils.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::utils )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::utils "${_IMPORT_PREFIX}/lib/libecal_utils.a" )

# Import target "eCAL::mon_plugin_lib" for configuration "Release"
set_property(TARGET eCAL::mon_plugin_lib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::mon_plugin_lib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_mon_plugin_lib.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::mon_plugin_lib )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::mon_plugin_lib "${_IMPORT_PREFIX}/lib/libecal_mon_plugin_lib.a" )

# Import target "eCAL::rec_addon_core" for configuration "Release"
set_property(TARGET eCAL::rec_addon_core APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::rec_addon_core PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_rec_addon_core.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::rec_addon_core )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::rec_addon_core "${_IMPORT_PREFIX}/lib/libecal_rec_addon_core.a" )

# Import target "eCAL::hdf5" for configuration "Release"
set_property(TARGET eCAL::hdf5 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(eCAL::hdf5 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libecal_hdf5.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS eCAL::hdf5 )
list(APPEND _IMPORT_CHECK_FILES_FOR_eCAL::hdf5 "${_IMPORT_PREFIX}/lib/libecal_hdf5.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
